# Festival database

The goal of this work is to get to know the handling of databases and to apply or deepen the learned skills. Accordingly, we are to develop a product connected to a database from the concept to the actual product, in order to go through all development steps once.

Another part of this work is to develop a graphical user interface (GUI), so that we also get an insight into the presentation of the data and, in our case, web development.


We have documented our project in our term paper, which can be found at [`/master/Belegarbeit_Festival_Datenbank.pdf`](https://gitlab.com/chrisvgt/c29-database/-/raw/master/Belegarbeit_Festival_Datenbank.pdf)

We do not guarantee the accuracy and completeness of the information in the paper.

<img src="overview.png" width="60%"></img>




