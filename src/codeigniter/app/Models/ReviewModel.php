<?php namespace App\Models;

use CodeIgniter\Model;

class ReviewModel extends Model{
    protected $table = 'reviews';
    protected $primaryKey = 'id';
    protected $allowedFields = ''; # fields which can be changed
    
    public function getReviewsForLoggedInUser(){
        $db = db_connect();

        $reviews = $db  ->table('reviews')
                        ->join('festivals', 'reviews.festivalID = festivals.id')
                        ->select('festivalRating, locationRating, text, created_at, name, festivalID')
                        ->where('userID', session()->get('id'))
                        ->orderBy('created_at', 'DESC')
                        ->get()
                        ->getResultArray();

        return $reviews;
    }
}
