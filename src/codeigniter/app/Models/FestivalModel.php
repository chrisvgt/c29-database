<?php namespace App\Models;

use CodeIgniter\Model;
$db = \Config\Database::connect();

class FestivalModel extends Model
{
    protected $table = 'festivals';
    protected $primaryKey = 'id';

/**
 * @brief function to get the next coming festivals, filtered by passed filters
 * 
 * genre filter is implemented as "and"-filter: a festival needs to have all selected genre tags to be listed
 * 
 * @return array containing an array of festivals and another array of ratings
 */
    public function getComingFestivals($month, $past, $type, $camping, $genres)
    {
        $db = db_connect(); //not using query builder because it doesn't seem to support nested joins without sql-hacks

        /*
         * filters have been ommitted for better readability
         *  
         *  select distinct f.*, c.country, t.price, COUNT(*) OVER() as numberReturned
         *  from festivals as f -- from festivals + (locations + countries) + (ticket_templates only Base Packages)
         *  inner join locations as l -- festivals + (locations + countries)
         *          inner join countries as c -- locations + countries
         *              on l.countryCode = c.countryCode
         *      on f.locationID = l.id
         *  inner join ticket_templates as t -- festivals
         *      on f.id = t.festivalID
         *      and t.option = 'Base Package' -- ticket_templates only Base Packages
         *  order by f.startDate;
        */

        $sqlCommand =  "SELECT distinct f.*, c.country, t.price, l.latitude, l.longitude, COUNT(*) OVER() AS numberReturned
                        FROM festivals AS f
                        INNER JOIN locations AS l
                            INNER JOIN countries AS c
                                ON l.countryCode = c.countryCode
                            ON f.locationID = l.id
                        INNER JOIN ticket_templates AS t
                            ON f.id = t.festivalID
                            AND t.option = 'Base Package'";

        //Leerzeichen vor SQL Teilcommands nicht vergessen :)
        if($past == false){
            $sqlCommand .= " and f.startDate >= now()";
        }

        if($month !== ""){
            $sqlCommand .= " and f.startDate < now() + interval ".$month." month";
        }

        if($type == "indoor"){
            $sqlCommand .= " and f.type = 'indoor'";
        }elseif($type == "outdoor"){
            $sqlCommand .= " and f.type = 'outdoor'";
        }

        if($camping == "1"){
            $sqlCommand .= " and f.withCamping = true";
        }elseif($camping == "0"){
            $sqlCommand .= " and f.withCamping = false";
        }

        if($genres !== NULL && count($genres) != 0){
            $sqlCommand .= " inner join genresAndFestivals as gaf on f.id = gaf.festivalID and (";

            $i = 0;
            foreach($genres as &$genre){
                $sqlCommand .= "gaf.genre = '" . $genre . "'";

                if(++$i !== count($genres)){ //every item except last gets concatenated with or operator
                    $sqlCommand .= " or ";
                }
            }
            unset($genre);

            $sqlCommand .= ")";
        }

        $sqlCommand .= " order by f.startDate;";

        $festivals = $db->query($sqlCommand)->getResultArray();

        $genres = $db   ->table('genres')
                        ->select('*')
                        ->get()
                        ->getResultArray();
        
        $results = [
            "festivals" => $festivals,
            "genres" => $genres,
            "cmd" => $sqlCommand,
        ];
        $db->close();
        return $results; 
    }

    /**
     * @brief generates an html string to display star based rating with 1-5 stars
     * 
     * @param numberOfStars float, structure: m+n0.5 where m and n are whole numbers (e.g. 3.5)
     * @param value optional parameter, if not passed, no value will be displayed after the stars; NULL if no rating was found, will display "no ratings yet"; only pass NULL (0) or 1-5, everything else might break the function :)  
     *
     * @return string html string showing stars and optionally a value for that rating behind the stars
     */
    public function getRatingDisplay($numberOfStars, $value = -1){
        $retString = "";
        
        for($i = $numberOfStars; $i > 0; --$i){
            if($i > 0.5){ //print full stars
                $retString = $retString . '<i class="fas fa-star"></i>';
            }elseif($i == 0.5){ //print half star (if applicable)
                $retString = $retString . '<i class="fas fa-star-half-alt"></i>';
            }
        }

        //print empty stars
        for($i = 5; $i > round($numberOfStars, 0, PHP_ROUND_HALF_UP); --$i){
            $retString = $retString . '<i class="far fa-star"></i>';
        }

        if($value >= 0){ //only show, if a value to show was passed
            $retString = $retString . '<span class="text-muted">';
            $retString = $retString . '<span>&nbsp;</span>'; //visual space
                if($value == 0){ //also applies to NULL
                    $retString = $retString . 'no ratings yet';
                }else{
                    $retString = $retString . $value;
                }
            $retString = $retString . '</span>';
        }

        return $retString;
    }

    /**
    * @brief get festival by id
    * 
    * @param integer festivalID
    * 
    * @return string array of data arrays, namely festival data in "festival", 
    * 
    */
    public function getFestival($id)
    {
        //get data from db
        $db = db_connect();
        $festival = $db ->table('festivals')
                        ->select('*')
                        ->where('id', $id)
                        ->get()
                        ->getResultArray()[0];

        $location = $db ->table('locations')
                        ->select('*')
                        ->where('id', $festival['locationID'])
                        ->get()
                        ->getResultArray()[0];

        $country = $db  ->table('countries')
                        ->select('country')
                        ->where('countryCode', $location['countryCode'])
                        ->get()
                        ->getResultArray()[0];

        $avgRating = $db->table('reviews')
                        ->selectAvg('festivalRating', 'avgFestivalRating')
                        ->selectAvg('locationRating', 'avgLocationRating')
                        ->where('festivalId', $id)
                        ->get()
                        ->getResultArray()[0];

        $ticketOptions = $db->table('ticket_templates')
                            ->select('*')
                            ->where('festivalID', $id)
                            ->get()
                            ->getResultArray();

        $reviews = $db  ->table('reviews')
                        ->select('reviews.*, users.firstName, users.lastName')
                        ->join('users', 'reviews.userID = users.id')
                        ->where('festivalId', $id)
                        ->get()
                        ->getResultArray();

        //query bc codeigniter does not support inner joins
        $genresSQLCommand = "select genre
                            from genres as g
                            right join genresAndFestivals as gaf
                                right join festivals as f
                                on f.id = gaf.festivalID
                            on 'g.name' = gaf.genre
                            where f.id = " . $id;

        $genres = $db->query($genresSQLCommand)->getResultArray();

        //manipulate data if needed
        $avgRatingNumbers['avgFestivalRating'] = round($avgRating['avgFestivalRating'], 1); //rounds to one significant digit after decimal point
        $avgRatingNumbers['avgLocationRating'] = round($avgRating['avgLocationRating'], 1); //rounds to one significant digit after decimal point
        $avgRatingNumbers['starFestivalRating'] = round($avgRating['avgFestivalRating'] * 2) / 2; //rounds to 0.5-values for displaying stars
        $avgRatingNumbers['starLocationRating'] = round($avgRating['avgLocationRating'] * 2) / 2; //rounds to 0.5-values for displaying stars
        $avgRatingString['festival'] = $this->getRatingDisplay($avgRatingNumbers['starFestivalRating'], $avgRatingNumbers['avgFestivalRating']); //generate nice html to display the rating
        $avgRatingString['location'] = $this->getRatingDisplay($avgRatingNumbers['avgLocationRating'], $avgRatingNumbers['starLocationRating']); //generate nice html to display the rating

        foreach($reviews as &$review){
            $review['rating'] = [
                'festival' => $this->getRatingDisplay(round($review['festivalRating'] * 2) / 2),
                'location' => $this->getRatingDisplay(round($review['locationRating'] * 2) / 2),
            ];
        }
        unset($review);

        $results = [
            'festival' => $festival,
            'location' => $location,
            'country' => $country,
            'genres' => $genres,
            'avgRating' => $avgRatingString,
            'tickets' => $ticketOptions,
            'reviews' => $reviews,
        ];

        $db->close();
        return $results; 
    }
}
