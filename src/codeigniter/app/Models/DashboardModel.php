<?php namespace App\Models;

use CodeIgniter\Model;

class DashboardModel extends Model
{
    # for QueryBuilder
    protected $table = 'festivals';
    protected $primaryKey = 'id';
    protected $allowedFields = ''; # fields which can be changed

    public function getTicketsForLoggedInUser(){
        $db = db_connect();
        $tickets = $db  ->table('tickets')
                        ->select('purchased_at, status, option, price, validFromDate, expirationDate, name, tt.festivalID')
                        ->join('ticket_templates as tt', 'tt.id = tickets.templateID')
                        ->join('festivals as f', 'tt.festivalID = f.id')
                        ->where('userID', session()->get('id'))
                        ->orderBy('validFromDate', 'ASC')
                        ->get()
                        ->getResultArray();
        
        //a ticket is marked as invalid if its either expired or has been invalidated in the database
        

        foreach($tickets as &$ticket){
            $ticket['invalid'] = ($ticket['status'] == 'invalid' || time() > strtotime($ticket['expirationDate']));
        }
        unset($ticket);

        return $tickets;
    }
}
