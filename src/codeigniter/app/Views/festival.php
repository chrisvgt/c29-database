<?php
    $startDate = date("d.m.Y", strtotime($festival['startDate']));
    $endDate = date("d.m.Y", strtotime($festival['endDate']));
    $formattedAddress = $location['street'] . " " . $location['streetNumber'] . ", " . $location['zipcode'] . " " .  $location['city'] . ", " . $country['country'];
?>

<div class="container">
    <div class="row">
        <?php if (isset($validation)): ?>
        <div class="col-10 mt-5">
            <div class="alert alert-danger" role="alert">
                <?= "Invalid request!" ?>
            </div>
        <?php endif; ?>
        <div class="col-12 col-sm-12 col-md-12 mt-5 pl-5 pt-4 pr-5 pb-2" id="outline">
            <h3><?= $festival['name'] ?> (<?= $startDate ?> - <?= $endDate ?>)</h3>
            <hr />
            <ul class="nav nav-tabs" role="tablist">
                <li class="nav-item"><a class="nav-link <?php echo (session()->getFlashdata('ticketTabSelected') === NULL && session()->getFlashdata('reviewTabSelected') === NULL) ? 'active' : ''; ?>" id="infos-tab" data-toggle="tab" role="tab"
                        aria-controls="infos" aria-selected="true" href="#infos">Infos</a></li>
                <li class="nav-item"><a class="nav-link <?php echo (session()->getFlashdata('ticketTabSelected') !== NULL) ? 'active' : ''; ?>" id="tickets-tab" data-toggle="tab" role="tab"
                        aria-controls="tickets" aria-selected="true" href="#tickets">Tickets</a></li>
                <li class="nav-item"><a class="nav-link <?php echo (session()->getFlashdata('reviewTabSelected') !== NULL) ? 'active' : ''; ?>" id="reviews-tab" data-toggle="tab" role="tab"
                        aria-controls="reviews" aria-selected="true" href="#reviews">Reviews</a></li>
            </ul>

            <div class="tab-content" id="nav-tabContent">
                <div id="infos" class="tab-pane fade <?php echo (session()->getFlashdata('ticketTabSelected') === NULL && session()->getFlashdata('reviewTabSelected') === NULL) ? ' show active' : ''; ?>" role="tabpanel" aria-labelledby="infos-tab">
                    <div class="container">
                        <div class="row">
                            <div class="col-12 col-sm-6 col-md-6 mt-2 pt-3 pb-3">
                                <div class="row">
                                    <div class="col mt-2 pt-3 pb-3">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">Rating</h4>
                                                <div class="card-text">
                                                    <div class="row">
                                                        <div class="col">
                                                            <span>Festival:</span>
                                                        </div>
                                                        <div class="col">
                                                            <?= $avgRating['festival'] ?>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col">
                                                            <span>Location:</span>
                                                        </div>
                                                        <div class="col">
                                                            <?= $avgRating['location'] ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col mt-2 pt-3 pb-3">
                                        <div class="card">
                                            <div class="card-body">
                                                <h4 class="card-title">Music</h4>
                                                <div class="card-text">
                                                    <span>
                                                        Genres: 
                                                        <span>
                                                            <?php
                                                                $i = 0;

                                                                if(count($genres) === 0){
                                                                    echo "unknown";
                                                                }else{
                                                                    foreach($genres as &$genre){
                                                                        echo $genre['genre'];
                                                                        if(++$i !== count($genres)){ //every item except last gets comma seperated
                                                                            echo ", ";
                                                                        }
                                                                    }
                                                                    unset($genre); //otherwise there might be bugs with genre still referencing to an item from genres
                                                                }
                                                            ?>
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col-12 col-sm-6 col-md-6 mt-2 pt-3 pb-3">
                                <div id="map" style="width: 100%; height: 100%;"></div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col mt-2 pt-3 pb-3">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Stay</h4>
                                        <div class="card-text">
                                            <span>Outdoors:
                                                <span>
                                                    <?php
                                                        if ($festival['type'] == 'indoor'){echo 'no, indoors';}
                                                        elseif ($festival['type'] == 'outdoor'){echo 'yes';}
                                                        else echo 'unknown';
                                                    ?>
                                                </span>
                                            </span>
                                        </div>
                                        <div class="card-text">
                                            <span>Camping:
                                                <span>
                                                    <?php
                                                        if ($festival['withCamping'] == "0"){echo 'no';}
                                                        elseif ($festival['withCamping'] == "1"){echo 'yes';}
                                                        else echo 'unknown';
                                                    ?>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="col mt-2 pt-3 pb-3">
                                <div class="card">
                                    <div class="card-body">
                                        <h4 class="card-title">Location</h4>
                                        <div class="card-text"><span>Name: <span><?= $location['name'] ?></span></span></div>
                                        <div class="card-text">
                                            <span>Address: <span><?= $formattedAddress ?></span></span></div>
                                        <div class="card-text"><span>Latitude north: <span><?= $location['latitude'] ?></span></span>
                                        </div>
                                        <div class="card-text"><span>Longitude east: <span><?= $location['longitude'] ?></span></span></div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>

                <div id="tickets" class="tab-pane fade <?php echo (session()->getFlashdata('ticketTabSelected') !== NULL) ? 'show active' : ''; ?>" role="tabpanel" aria-labelledby="tickets-tab">
                    <div class="container">
                        <?php //generate boxes for each ticket template with php
                            if(session()->getFlashdata('purchaseConfirmation')){
                                echo '<div class="row">';
                                echo    '<div class="col-12 mt-2 pt-3 pb-3">';
                                if(session()->getFlashdata('purchaseConfirmation') === "Ticket purchase successful!"){
                                    echo    '<div class="alert alert-success" role="alert">';
                                }else{
                                    echo    '<div class="alert alert-danger" role="alert">';
                                }
                                echo            session()->getFlashdata('purchaseConfirmation');
                                echo        '</div>';
                                echo    '</div>';
                                echo '</div>';
                            }
                            
                            foreach($tickets as &$ticket){
                                $validFromDate = date("d.m.Y H:i", strtotime($ticket['validFromDate'])); //no seconds
                                $expirationDate = date("d.m.Y H:i", strtotime($ticket['expirationDate'])); //no seconds

                                echo '<div class="row">';
                                    echo '<div class="col-12 mt-2 pt-3 pb-3">';
                                        echo '<div class="card border-secondary">';
                                            echo '<div class="card-body">';
                                                echo '<div class="row">';
                                                    echo '<div class="col">';
                                                        echo '<h4 class="card-title">' . $ticket['option'] . '</h4>'; //name
                                                    echo '</div>';
                                                echo '</div>';
                                                echo '<div class="row">';
                                                    echo '<div class="col">';
                                                        echo '<div class="card-text"><span>valid from: <span>' . $validFromDate . '</span></span></div>';
                                                    echo '</div>';
                                                    echo '<div class="col">';
                                                        echo '<div class="card-text"><span>valid until: <span>' . $expirationDate . '</span></span></div>';
                                                    echo '</div>';
                                                echo '</div>';
                                            echo '</div>';
                                            echo '<div class="card-footer">';
                                                //only one of these buttons needs to be generated
                                                echo '<form action="Festival/buytickets" method="post" id="ticketForm">';
                                                    echo '<input type="hidden" id="fID" name="fID" value="' . $ticket['festivalID'] . '"/>'; //add id of festival (fID)
                                                    echo '<input type="hidden" id="ttID" name="ttID" value="' . $ticket['id'] . '"/>'; //add id of ticket template (ttID)
                                                    echo '<div class="row">';
                                                        echo '<div class="col">';
                                                            if($ticket['soldTickets'] >= $ticket['totalTickets']){ //sold out
                                                                echo '<button class="btn btn-secondary" disabled="true"><span>sold out</span></button>';
                                                            }else{
                                                                echo '<button type="submit" class="btn btn-primary"><span>Buy for <span>' . $ticket['price'] . '€</span></span></button>';
                                                            }
                                                        echo '</div>';
                                                    echo '</div>';
                                                echo '</form>';
                                            echo '</div>';
                                        echo '</div>';
                                    echo '</div>';
                                echo '</div>';
                            }

                            unset($ticket); //unset to avoid some reference errors
                        ?>
                    </div>
                </div>

                <div id="reviews" class="tab-pane fade <?php echo (session()->getFlashdata('reviewTabSelected') !== NULL) ? 'show active' : ''; ?>" role="tabpanel" aria-labelledby="reviews-tab">
                    <div class="container">
                        <?php
                            if(session()->getFlashdata('reviewPostConfirmation')){
                                echo '<div class="row">';
                                echo    '<div class="col-12 mt-2 pt-3 pb-3">';
                                if(session()->getFlashdata('reviewPostConfirmation') === "Review posted successfully!"){
                                    echo    '<div class="alert alert-success" role="alert">';
                                }else{
                                    echo    '<div class="alert alert-danger" role="alert">';
                                }
                                echo            session()->getFlashdata('reviewPostConfirmation');
                                echo        '</div>';
                                echo    '</div>';
                                echo '</div>';
                            }
                        ?>
                        
                        <div class="row">
                            <div class="col-12 mt-2 pt-3 pb-3">
                                <div class="card border-secondary">
                                    <div class="card-body">
                                        <h4 class="card-title">Rating</h4>
                                        <div class="card-text">
                                            <div class="row">
                                                <div class="col col-md-2">
                                                    <span>Festival:</span>
                                                </div>
                                                <div class="col col-md-4">
                                                    <?= $avgRating['festival'] ?>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col col-md-2">
                                                    <span>Location:</span>
                                                </div>
                                                <div class="col col-md-4">
                                                    <?= $avgRating['location'] ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- if user didn't leave a review yet and owns a ticket and the festival has started -->
                        <div class="row">
                            <div class="col-12 mt-2 pt-3 pb-3">
                                <div class="card border-secondary">
                                    <div class="card-body">
                                        <h4 class="card-title">Write review</h4>
                                        <form action="Festival/writereview" method="post" id="reviewForm" onsubmit="return addPostData();" required="">
                                            <div class="row">
                                                <div class="col">
                                                    <div class="from-group">
                                                        <div id="festivalRating">
                                                            <label for="festivalRating">Festival: </label>
                                                            <span class="dynamicRating far fa-star" value="1"></span>
                                                            <span class="dynamicRating far fa-star" value="2"></span>
                                                            <span class="dynamicRating far fa-star" value="3"></span>
                                                            <span class="dynamicRating far fa-star" value="4"></span>
                                                            <span class="dynamicRating far fa-star" value="5"></span>
                                                        </div>
                                                        <input name="festivalRating" type="hidden" value=""/>
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="from-group">
                                                        <div id="locationRating">
                                                            <label for="locationRating">Location: </label>
                                                            <span class="dynamicRating far fa-star" value="1"></span>
                                                            <span class="dynamicRating far fa-star" value="2"></span>
                                                            <span class="dynamicRating far fa-star" value="3"></span>
                                                            <span class="dynamicRating far fa-star" value="4"></span>
                                                            <span class="dynamicRating far fa-star" value="5"></span>
                                                        </div>
                                                        <input name="locationRating" type="hidden" value=""/>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <div class="from-group">
                                                        <textarea class="form-control" id="reviewText"
                                                            name="reviewText"
                                                            placeholder="please share your thoughts"
                                                            maxlength="1000"></textarea>
                                                        <small class="text-muted">1000 characters</small>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row">
                                                <div class="col">
                                                    <input name="id" id="id" type="hidden" value="<?= $festival['id'] ?>"/>
                                                    <button type="submit" class="btn btn-primary">Post</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <!-- all of this needs to be created by php -->
                        <?php
                            if(count($reviews) == 0){ //show review-missing-text if there are no reviews
                                echo '<div class="row">';
                                echo     '<div class="col-12 mt-2 pt-3 pb-3">';
                                echo         '<div class="card border-secondary">';
                                echo             '<div class="card-body">';
                                echo                 '<div class="row">';
                                echo                     '<div class="col">';
                                echo                         '<h4 class="card-title">No reviews yet. Be the first to write one!</h4>';
                                echo                     '</div>';
                                echo                 '</div>';
                                echo             '</div>';
                                echo         '</div>';
                                echo     '</div>';
                                echo '</div>';
                            }else{
                                echo '<div class="row">';
                                echo     '<div class="col-12 mt-2 pt-3 pb-3">';
                                echo         '<div class="text-muted text-right"><span>' . count($reviews) . ' reviews </span></div>';
                                echo     '</div>';
                                echo '</div>';
                            }

                            foreach($reviews as &$review){ //triggers only if there are reviews
                                $postDate = date("d.m.Y H:i", strtotime($review['created_at'])); //no seconds

                                echo '<div class="row">';
                                echo     '<div class="col-12 mt-2 pt-3 pb-3">';
                                echo         '<div class="card border-secondary">';
                                echo             '<div class="card-body">';
                                echo                 '<div class="row">';
                                echo                     '<div class="col-8">';
                                echo                         '<h4 class="card-title">' . $review['firstName'] . ' ' . $review['lastName'] . '</h4>';
                                echo                         '<div class="card-text"><span>' . htmlspecialchars($review['text'], ENT_QUOTES, 'UTF-8') . '</span></div>';
                                echo                     '</div>';
                                echo                     '<div class="col-4">';
                                echo                         '<div class="card-text">';
                                echo                             '<div class="row">';
                                echo                                 '<div class="col-12 col-lg">';
                                echo                                     '<span>Festival:</span>';
                                echo                                 '</div>';
                                echo                                 '<div class="col-12 col-lg">';
                                echo                                     $review['rating']['festival'];
                                echo                                 '</div>';
                                echo                             '</div>';
                                echo                             '<div class="row">';
                                echo                                 '<div class="col-12 col-lg">';
                                echo                                     '<span>Location:</span>';
                                echo                                 '</div>';
                                echo                                 '<div class="col-12 col-lg">';
                                echo                                     $review['rating']['location'];
                                echo                                 '</div>';
                                echo                             '</div>';
                                echo                         '</div>';
                                echo                     '</div>';
                                echo                 '</div>';
                                echo             '</div>';
                                echo             '<div class="card-footer">';
                                echo                 '<small class="text-muted">' . $postDate . '</small>';
                                echo             '</div>';
                                echo         '</div>';
                                echo     '</div>';
                                echo '</div>';
                            }
                            unset($review);
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>