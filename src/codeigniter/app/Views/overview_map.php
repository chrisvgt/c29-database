<!-- loads google map scripts -->
<!-- currently the top two scripts are not deferred because this is the easiest way i found to pass $festivals to the init function -->
<!-- the api key for the google api has to be placed in the file under "app/Views/googleAPIKey". this is done, to prevent logging the api key in this repository. -->
<!-- Google Maps -->
<script src="/assets/js/map.js"></script>
<script>
    var gmapsInit = initMap.bind(null, <?= json_encode($festivals) ?>, null, null, true); //prebind festival data to the function that then will be called back from the google maps api
</script>
<script src="https://maps.googleapis.com/maps/api/js?key=<?= file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'googleAPIKey', true) ?>&callback=gmapsInit&libraries=&v=weekly" defer></script>