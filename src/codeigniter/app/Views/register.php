<div class="container">
    <div class="row">
         <div class="col-12 col-sm-8 offset-sm-2 col-md-6 offset-md-3 mt-5 pt-3 pb-3" id="outline">
            <div class="container">
                <h3>Register</h3>
                <hr>
                <form class="" action="/register" method="post">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="firstname">First Name</label>
                                <input type="text" class="form-control" name="firstname" id="firstname"
                                    value="<?= set_value('firstname') ?>" />
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="lastname">Last Name</label>
                                <input type="text" class="form-control" name="lastname" id="lastname"
                                    value="<?= set_value('lastname') ?>" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="dob">DOB</label>
                                <input type="date" class="form-control" name="dob" id="dob" value="<?= set_value('dob')?>">
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="countryCode">Country</label>
                                <select class="selectpicker form-control" id="countryCode" name="countryCode" data-live-search="true" title="please select" multiple data-max-options="1">
                                    <?php
                                        foreach($countries as &$country){
                                            echo '<option' . (($newCountryCode !== NULL && $country['countryCode'] == $newCountryCode) ? ' selected' : '') . ' value="' . $country['countryCode'] . '">' . $country['country'] . '</option>';
                                        }
                                        unset($country);
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-9">
                            <div class="form-group">
                                <label for="city">City</label>
                                <input type="text" class="form-control" name="city" id="city" value="<?= set_value('city') ?>">
                            </div>
                        </div>
                        <div class="col-12 col-sm-3">
                            <div class="form-group">
                                <label for="zipcode">Zipcode</label>
                                <input type="text" class="form-control" name="zipcode" id="zipcode" value="<?= set_value('zipcode') ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-9">
                            <div class="form-group">
                                <label for="street">Street</label>
                                <input type="text" class="form-control" name="street" id="street" value="<?= set_value('street') ?>">
                            </div>
                        </div>
                        <div class="col-12 col-sm-3">
                            <div class="form-group">
                                <label for="streetNumber">Street Number</label>
                                <input type="text" class="form-control" name="streetNumber" id="streetNumber" value="<?= set_value('streetNumber') ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="text"  class="form-control" name="email" id="email" value="<?= set_value('email') ?>" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password" id="password" value="" />
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="password_confirm">Confirm Password</label>
                                <input type="password" class="form-control" name="password_confirm"
                                    id="password_confirm" value="" />
                            </div>
                        </div>
                    </div>

                    <?php if (isset($validation)): ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="alert alert-danger" role="alert">
                                    <?= $validation->listErrors() ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>

                    <div class="row">
                        <div class="col-12 col-sm-4">
                            <button type="submit" class="btn btn-primary">Register</button>
                        </div>
                        <div class="col-12 col-sm-8 text-right">
                            <a href="/login">Already have an account?</a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
