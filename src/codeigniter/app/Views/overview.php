<div class="container">
    <div class="row">
        <?php if (isset($validation)): ?>
        <div class="col-10 mt-5">
            <div class="alert alert-danger" role="alert">
                <?= "Invalid request!" ?>
            </div>
        </div>
        <?php endif; ?>
        <div class="col-12 col-sm-6 col-md-6 mt-5 pl-5 pt-4 pr-5 pb-2" id="outline">
            <h3 class="neonheading">Filter</h3>
            <hr/>
            <form action="/" method="post">
                <div class="row">
                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="numMonth">next festivals</label>
                            <select class="form-control selectpicker" id="numMonth" name="numMonth">
                                <option <?php if($filters['numMonth'] ==   "" || $filters === NULL){echo 'selected';}?> value="">all</option>
                                <option <?php if($filters['numMonth'] ==  "1" && $filters !== NULL){echo 'selected';}?> value="1">1 month</option>
                                <option <?php if($filters['numMonth'] ==  "3" && $filters !== NULL){echo 'selected';}?> value="3">3 months</option>
                                <option <?php if($filters['numMonth'] ==  "6" && $filters !== NULL){echo 'selected';}?> value="6">6 months</option>
                                <option <?php if($filters['numMonth'] == "12" && $filters !== NULL){echo 'selected';}?> value="12">12 months</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="type">type</label>
                            <select class="form-control selectpicker" id="type" name="type">
                                <option <?php if($filters['type'] == "" || $filters === NULL){echo 'selected';}?> value="">all</option>
                                <option <?php if($filters['type'] == "indoor" && $filters !== NULL){echo 'selected';}?> value="indoor">indoors</option>
                                <option <?php if($filters['type'] == "outdoor" && $filters !== NULL){echo 'selected';}?> value="outdoor">outdoors</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-12 col-sm-4">
                        <div class="form-group">
                            <label for="camping">camping</label>
                            <select class="form-control selectpicker" id="camping" name="camping">
                                <option <?php if($filters['camping'] == "" || $filters === NULL){echo 'selected';}?> value="">all</option>
                                <option <?php if($filters['camping'] == "1" && $filters !== NULL){echo 'selected';}?> value="1">with</option>
                                <option <?php if($filters['camping'] == "0" && $filters !== NULL){echo 'selected';}?> value="0">without</option>
                            </select>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label for="genres">genres</label>
                            <select class="form-control selectpicker" id="genres" name="genres[]" multiple data-live-search="true" title="all">
                                <?php
                                    //name needs to end in []! only then, all genres options are passed as an array, instead of just the "last" option
                                    foreach($genres as &$genre){
                                        echo '<option ' . (($filters !== NULL && $filters['genres'] !== NULL && in_array($genre['name'], $filters['genres'])) ? 'selected' : '') . ' value="' . $genre['name'] . '">' . $genre['name'] . '</option>';
                                    }
                                    unset($genre);
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="col-12">
                        <div class="form-group">
                            <label class="checkbox-inline text-muted">
                                <input type="checkbox" value="showPast" name="showPast" <?php if($filters['showPast'] == "showPast"){echo 'checked';}?>/>
                                show all past festivals
                            </label>                                    
                        </div>
                    </div>

                    <div class="col-12 col-sm-6">
                        <div class="form-group">
                            <button type="submit" class="btn btn-primary">apply</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>

        <div class="col-12 col-sm-8 col-md-6 mt-5 pt-3 pb-3">
            <div id="map" style="width: 100%; height: 100%;"></div>
        </div>
    </div>

    <div class="row">
        <div class="col-12 col-sm-12 col-md-12 mt-5 pl-5 pt-4 pr-5 pb-2" id="outline">
            <h3 class="neonheading">Events</h3>
            <hr />
            <div class="table-responsive-md">
                <table class="table text-center table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>Name</th>
                            <th>Start</th>
                            <th>End</th>
                            <th>Outdoor</th>
                            <th>Camping</th>
                            <th>Price</th>
                            <th>Country</th>
                        </tr>
                    </thead>
                    <tbody>                  
                        <?php
                        foreach ($festivals as $item)
                        {
                            // convert mysql date to d.m.y
                            $tmp = strtotime($item['startDate']);
                            $startDate = date("d.m.Y", $tmp);
                            $tmp = strtotime($item['endDate']);
                            $endDate = date("d.m.Y", $tmp);

                            // convert bools from type and withCamping to string
                            if ($item['type'] == 'outdoor'){$type = '<i class="fas fa-check"></i>';}
                            elseif ($item['type'] == 'indoor'){ $type = '<i class="fas fa-times"></i>';}
                            else $type = '<i></i>';
                            
                            if ($item['withCamping'] == 1){$withCamping = '<i class="fas fa-check"></i>';}
                            elseif ($item['withCamping'] == 0){ $withCamping = '<i class="fas fa-times"></i>';}
                            else $withCamping = '<i></i>';
                            
                            echo '<tr>';
                            echo '<td class="text-left"><a href="/festival?id='.$item['id'].'">' .$item['name']. '</a></td>';
                            echo '<td>' .$startDate. '</td>';
                            echo '<td>' .$endDate. '</td>';
                            echo '<td>' .$type. '</td>';
                            echo '<td>' .$withCamping. '</td>';
                            echo '<td>' .$item['price']. '€</td>';
                            echo '<td>' .$item['country']. '</td>';
                            echo '</tr>';
                        }
                        ?>
                    </tbody>
                </table>
            </div>
            <div class="container mb-5">
                <div class="text-right"><?php echo count($festivals);?> Entries</div>
            </div>
        </div>
    </div>
</div>