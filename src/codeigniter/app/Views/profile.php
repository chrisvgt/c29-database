<div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 mt-5 pl-5 pt-4 pr-5 pb-2" id="outline">
                <h3 class="neonheading">Settings</h3>
                <hr />
                <?php if (session()->get('success')): ?>
                <div class="alert alert-success" role="alert">
                    <?= session()->get('success') ?>
                </div>
                <?php endif; ?>
                <form class="" action="/profile" method="post">
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="firstname">First Name</label>
                                <input type="text" class="form-control" name="firstname" id="firstname"
                                    value="<?= set_value('firstname', $user['firstname']) ?>" />
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="lastname">Last Name</label>
                                <input type="text" class="form-control" name="lastname" id="lastname"
                                    value="<?= set_value('lastname', $user['lastname']) ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="dob">DOB</label>
                                <input type="date" class="form-control" name="dob" id="dob" value="<?= set_value('dob',$user['dob'])?>">
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <div class="form-group">
                                    <label for="countryCode">Country</label>
                                    <select class="selectpicker form-control" id="countryCode" name="countryCode" data-live-search="true" title="please select" multiple data-max-options="1">
                                        <?php
                                            foreach($countries as &$country){
                                                //tldr: always show the latest selected country and users country from database, if nothing has been selected so far.
                                                //this is important when users try to change their profile, but that fails because some filter rule is not adhered to.
                                                //select the current country, if newCountry is set and matches the current country. if new country is set, but doesnt match the current country, dont select. else use users country as fallback: if newCountry is not set and the previous users country matches the current country, mark it as selected.
                                                echo '<option' . (($newCountryCode !== NULL && $country['countryCode'] == $newCountryCode) ? ' selected' : (($newCountryCode === NULL && $user['countryCode'] !== NULL && $country['countryCode'] == $user['countryCode']) ? ' selected' : '')) . ' value="' . $country['countryCode'] . '">' . $country['country'] . '</option>';
                                            }
                                            unset($country);
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-9">
                            <div class="form-group">
                                <label for="city">City</label>
                                <input type="text" class="form-control" name="city" id="city" value="<?= set_value('city', $user['city']) ?>">
                            </div>
                        </div>
                        <div class="col-12 col-sm-3">
                            <div class="form-group">
                                <label for="zipcode">Zipcode</label>
                                <input type="text" class="form-control" name="zipcode" id="zipcode" value="<?= set_value('zipcode', $user['zipcode']) ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-9">
                            <div class="form-group">
                                <label for="street">Street</label>
                                <input type="text" class="form-control" name="street" id="street" value="<?= set_value('street', $user['street']) ?>">
                            </div>
                        </div>
                        <div class="col-12 col-sm-3">
                            <div class="form-group">
                                <label for="streetNumber">Street Number</label>
                                <input type="text" class="form-control" name="streetNumber" id="streetNumber" value="<?= set_value('streetNumber', $user['streetNumber']) ?>">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="email">Email Address</label>
                                <input type="text" class="form-control" readonly="true" id="email"
                                    value="<?= $user['email'] ?>" />
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" class="form-control" name="password" id="password" value="" />
                            </div>
                        </div>
                        <div class="col-12 col-sm-6">
                            <div class="form-group">
                                <label for="password_confirm">Confirm Password</label>
                                <input type="password" class="form-control" name="password_confirm"
                                    id="password_confirm" value="" />
                            </div>
                        </div>
                    </div>
                    <?php if (isset($validation)): ?>
                        <div class="row">
                            <div class="col-12">
                                <div class="alert alert-danger" role="alert">
                                    <?= $validation->listErrors() ?>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                    <div class="row">
                        <div class="col text-center">
                            <button type="submit" class="btn btn-primary">Update</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>