<div class="container">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12 mt-5 pl-5 pt-4 pr-5 pb-2" id="outline">
                <h3 class="neonheading">Dashboard</h3>
                <h4>Hello, <em><?= session()->get('firstname') ?> <?= session()->get('lastname') ?></em></h4>
                <hr />
                <ul class="nav nav-tabs" role="tablist">
                    <li class="nav-item"><a class="nav-link active" id="tickets-tab" data-toggle="tab" role="tab"
                            aria-controls="tickets" aria-selected="true" href="#tickets">Tickets</a></li>
                    <li class="nav-item"><a class="nav-link" id="reviews-tab" data-toggle="tab" role="tab"
                            aria-controls="reviews" aria-selected="true" href="#reviews">Reviews</a></li>
                </ul>

                <div class="tab-content" id="nav-tabContent">
                    <div id="tickets" class="tab-pane fade show active" role="tabpanel" aria-labelledby="tickets-tab">
                        <div class="container">
                            <?php
                                if(count($tickets) == 0){ //own no tickets
                                    echo '<div class="row">';
                                    echo     '<div class="col-12 mt-2 pt-3 pb-3">';
                                    echo         '<div class="card border-secondary">';
                                    echo             '<div class="card-body">';
                                    echo                 '<div class="row">';
                                    echo                     '<div class="col">';
                                    echo                         '<h4 class="card-title">You own no tickets, time to get out there and visit some festivals!</h4>';
                                    echo                     '</div>';
                                    echo                 '</div>';
                                    echo             '</div>';
                                    echo         '</div>';
                                    echo     '</div>';
                                    echo '</div>';
                                }else{
                                    //non expired tickets
                                    foreach($tickets as &$ticket){
                                        if(!$ticket['invalid']){
                                            $validFromDate = date("d.m.Y H:i", strtotime($ticket['validFromDate'])); //no seconds
                                            $expirationDate = date("d.m.Y H:i", strtotime($ticket['expirationDate'])); //no seconds
                                            $boughtOnDate = date("d.m.Y H:i", strtotime($ticket['purchased_at'])); //no seconds

                                            echo '<div class="row">';
                                            echo     '<div class="col-12 mt-2 pt-3 pb-3">';
                                            echo         '<div class="card border-secondary">';
                                            echo             '<div class="card-body">';
                                            echo                 '<div class="row">';
                                            echo                     '<div class="col">';
                                            echo                         '<h4 class="card-title"><a href="/festival?id=' . $ticket['festivalID'] . '">' . $ticket['name'] . '</a>: ' . $ticket['option'] . '</h4>';
                                            echo                     '</div>';
                                            echo                 '</div>';
                                            echo                 '<div class="row">';
                                            echo                     '<div class="col">';
                                            echo                         '<div class="card-text"><span>valid from: </span><span>' . $validFromDate . '</span></div>';
                                            echo                     '</div>';
                                            echo                     '<div class="col">';
                                            echo                         '<div class="card-text"><span>valid until: </span><span>' . $expirationDate . '</span></div>';
                                            echo                     '</div>';
                                            echo                 '</div>';
                                            echo             '</div>';
                                            echo             '<div class="card-footer">';
                                            echo                 '<small class="text-muted"><span>bought on <span>' . $boughtOnDate . '</span> for <span>' . $ticket['price'] . '€</span></span>';
                                            echo                 '</small>';
                                            echo             '</div>';
                                            echo         '</div>';
                                            echo     '</div>';
                                            echo '</div>';
                                        }
                                    }
                                    unset($ticket);

                                    //expired / invalid tickets
                                    foreach($tickets as &$ticket){
                                        if($ticket['invalid']){
                                            $validFromDate = date("d.m.Y H:i", strtotime($ticket['validFromDate'])); //no seconds
                                            $expirationDate = date("d.m.Y H:i", strtotime($ticket['expirationDate'])); //no seconds
                                            $boughtOnDate = date("d.m.Y H:i", strtotime($ticket['purchased_at'])); //no seconds

                                            echo '<div class="row">';
                                            echo     '<div class="col-12 mt-2 pt-3 pb-3">';
                                            echo         '<div class="card">';
                                            echo             '<div class="card-body">';
                                            echo                 '<div class="row">';
                                            echo                     '<div class="col">';
                                            echo                         '<h4 class="card-title text-muted"><a href="/festival?id=' . $ticket['festivalID'] . '">' . $ticket['name'] . '</a>: ' . $ticket['option'] . ' (expired)</h4>';
                                            echo                     '</div>';
                                            echo                 '</div>';
                                            echo                 '<div class="row">';
                                            echo                     '<div class="col">';
                                            echo                         '<div class="card-text text-muted"><span>valid from: </span><span>' . $validFromDate . '</span></div>';
                                            echo                     '</div>';
                                            echo                     '<div class="col">';
                                            echo                         '<div class="card-text text-muted"><span>valid until: </span><span>' . $expirationDate . '</span></div>';
                                            echo                     '</div>';
                                            echo                 '</div>';
                                            echo             '</div>';
                                            echo             '<div class="card-footer">';
                                            echo                 '<small class="text-muted"><span>bought on <span>' . $boughtOnDate . '</span> for <span>' . $ticket['price'] . '€</span></span>';
                                            echo                 '</small>';
                                            echo             '</div>';
                                            echo         '</div>';
                                            echo     '</div>';
                                            echo '</div>';
                                        }
                                    }
                                    unset($ticket);
                                }
                            ?>
                        </div>
                    </div>

                    <div id="reviews" class="tab-pane fade" role="tabpanel" aria-labelledby="reviews-tab">
                        <div class="container">
                            <?php
                                if(count($reviews) == 0){ //own no tickets
                                    echo '<div class="row">';
                                    echo     '<div class="col-12 mt-2 pt-3 pb-3">';
                                    echo         '<div class="card border-secondary">';
                                    echo             '<div class="card-body">';
                                    echo                 '<div class="row">';
                                    echo                     '<div class="col">';
                                    echo                         '<h4 class="card-title">You haven\'t written any reviews yet. Go and share your thoughts!</h4>';
                                    echo                     '</div>';
                                    echo                 '</div>';
                                    echo             '</div>';
                                    echo         '</div>';
                                    echo     '</div>';
                                    echo '</div>';
                                }else{
                                    foreach($reviews as &$review){
                                        $postDate = date("d.m.Y H:i", strtotime($review['created_at'])); //no seconds

                                        echo '<div class="row">';
                                        echo     '<div class="col-12 mt-2 pt-3 pb-3">';
                                        echo         '<div class="card border-secondary">';
                                        echo             '<div class="card-body">';
                                        echo                 '<div class="row">';
                                        echo                     '<div class="col-8">';
                                        echo                         '<h4 class="card-title"><a href="/festival?id=' . $review['festivalID'] . '">' . $review['name'] . '</a></h4>';
                                        echo                         '<div class="card-text"><span>' . htmlspecialchars($review['text'], ENT_QUOTES, 'UTF-8') . '</span></div>';
                                        echo                     '</div>';
                                        echo                     '<div class="col-4">';
                                        echo                         '<div class="card-text">';
                                        echo                             '<div class="row">';
                                        echo                                 '<div class="col-12 col-lg">';
                                        echo                                     '<span>Festival:</span>';
                                        echo                                 '</div>';
                                        echo                                 '<div class="col-12 col-lg">';
                                        echo                                     $review['rating']['festival'];
                                        echo                                 '</div>';
                                        echo                             '</div>';
                                        echo                             '<div class="row">';
                                        echo                                 '<div class="col-12 col-lg">';
                                        echo                                     '<span>Location:</span>';
                                        echo                                 '</div>';
                                        echo                                 '<div class="col-12 col-lg">';
                                        echo                                     $review['rating']['location'];
                                        echo                                 '</div>';
                                        echo                             '</div>';
                                        echo                         '</div>';
                                        echo                     '</div>';
                                        echo                 '</div>';
                                        echo             '</div>';
                                        echo             '<div class="card-footer">';
                                        echo                 '<small class="text-muted">' . $postDate . '</small>';
                                        echo             '</div>';
                                        echo         '</div>';
                                        echo     '</div>';
                                        echo '</div>';
                                    }
                                    unset($review);
                                }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>