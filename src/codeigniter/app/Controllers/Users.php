<?php namespace App\Controllers;

use App\Models\UserModel;

class Users extends BaseController
{
	private function setUserSession($user)
	{
		$data = 
		[
			'id' => $user['id'],
			'firstname' => $user['firstname'],
			'lastname' => $user['lastname'],
			'email' => $user['email'],
			'zipcode' => $user['zipcode'],
			'city' => $user['city'],
			'countryCode' => $user['countryCode'],
			'isLoggedIn' => true,
		];

		session()->set($data);
		return true;
	}

	public function index()
	{
		$data = [];
		helper(['form']);

		echo view('templates/header', $data);

		if ($this->request->getMethod() == 'post')
		{
			$rules = 
			[
				'email' => 'required|min_length[6]|max_length[50]|valid_email',
				'password' => 'required|min_length[8]|max_length[255]|validateUser[email,password]',
			];

			$errors = 
			[
				'password' => 
				[
					'validateUser' => 'Email or Password don\'t match'
				]
			];

			if (!$this->validate($rules, $errors)) 
			{
				$data['validation'] = $this->validator;
			}
			else
			{
				$model = new UserModel();
				$user = $model->where('email', $this->request->getVar('email'))->first();
				
				$this->setUserSession($user);
				return redirect()->to('/dashboard');
			}
		}

		echo view('login');
		echo view('templates/footer');
	}

	public function register(){
		$headerData = [];
		$data = [];
		helper(['form']);

		$model = new UserModel();

		if ($this->request->getMethod() == 'post')
		{
			//let's do the validation here
			$rules =
			[
				'firstname' => 'required|min_length[3]|max_length[20]',
				'lastname' => 'required|min_length[3]|max_length[20]',
				'email' => 'required|min_length[6]|max_length[50]|valid_email|is_unique[users.email]',
				'countryCode' => 'required|min_length[2]|max_length[2]',
				'dob' => 'required|min_length[10]|max_length[10]',
				'city' => 'required|min_length[2]|max_length[50]',
				'zipcode' => 'required|min_length[2]|max_length[20]',
				'street' => 'required|min_length[5]|max_length[50]',
				'streetNumber' => 'required|min_length[1]|max_length[10]',
				'password' => 'required|min_length[8]|max_length[255]',
				'password_confirm' => 'matches[password]',
			];

			if (! $this->validate($rules))
			{
				$headerData['validation'] = $this->validator;
			}
			else
			{
				$newData =
				[
					'firstname' => $this->request->getVar('firstname'),
					'lastname' => $this->request->getVar('lastname'),
					'email' => $this->request->getVar('email'),
					'password' => $this->request->getVar('password'),
					'countryCode' => $this->request->getVar('countryCode'),
					'dob' => $this->request->getVar('dob'),
					'city' => $this->request->getVar('city'),
					'zipcode' => $this->request->getVar('zipcode'),
					'street' => $this->request->getVar('street'),
					'streetNumber' => $this->request->getVar('streetNumber'),
				];
				$model->save($newData);
				$session = session();
				$session->setFlashdata('success', 'Successful Registration');
				return redirect()->to('/login');
			}
		}

		$data['countries'] = $model->getKnownCountries();
		if ($this->request->getMethod() == 'post'){
			$data['newCountryCode'] = $this->request->getVar('countryCode');
		}

		echo view('templates/header', $headerData);
		echo view('register', $data);
		echo view('templates/footer');
	}

	public function profile(){
		
		$data = [];
		helper(['form']);
		$model = new UserModel();

		if ($this->request->getMethod() == 'post')
		{
			//let's do the validation here
			$rules =
			[
				'firstname' => 'required|min_length[3]|max_length[20]',
				'lastname' => 'required|min_length[3]|max_length[20]',
				'countryCode' => 'required|min_length[2]|max_length[2]',
				'dob' => 'required|min_length[10]|max_length[10]',
				'city' => 'required|min_length[2]|max_length[50]',
				'zipcode' => 'required|min_length[2]|max_length[20]',
				'street' => 'required|min_length[5]|max_length[50]',
				'streetNumber' => 'required|min_length[1]|max_length[10]',
			];

			if($this->request->getPost('password') != '')
			{
				$rules['password'] = 'required|min_length[8]|max_length[255]';
				$rules['password_confirm'] = 'matches[password]';
			}


			if (! $this->validate($rules))
			{
				$data['validation'] = $this->validator;
			}
			else
			{
				$newData =
				[
					'id' => session()->get('id'),
					'firstname' => $this->request->getVar('firstname'),
					'lastname' => $this->request->getVar('lastname'),
					'countryCode' => $this->request->getVar('countryCode'),
					'dob' => $this->request->getVar('dob'),
					'city' => $this->request->getVar('city'),
					'zipcode' => $this->request->getVar('zipcode'),
					'street' => $this->request->getVar('street'),
					'streetNumber' => $this->request->getVar('streetNumber'),
				];
				if($this->request->getPost('password') != '')
				{
					$newData['password'] = $this->request->getPost('password');
				}
				$model->save($newData);
				session()->setFlashdata('success', 'Successfully Updated');
				return redirect()->to('/profile');
			}
		}

		$data['countries'] = $model->getKnownCountries();
		if ($this->request->getMethod() == 'post'){ //all other data is kept via "setValue()" but that method seems not applicable to this task
			$data['newCountryCode'] = $this->request->getVar('countryCode');
		}
		$data['user'] = $model->where('id', session()->get('id'))->first();
		echo view('templates/header', $data);
		echo view('profile');
		echo view('templates/footer');
	}

	public function logout()
	{
		session()->destroy();
		return redirect()->to('/');
	}

	//--------------------------------------------------------------------

}
