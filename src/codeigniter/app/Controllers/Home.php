<?php namespace App\Controllers;

use App\Models\FestivalModel;

class Home extends BaseController
{
	public function index()
	{
		$data = [];
		helper(['form']);
		$festivalModel = new FestivalModel();

		if ($this->request->getMethod() == 'post'){
			$rules = [
				'camping' => 'max_length[1]',
				'type' => 'max_length[7]',
				'numMonth' => 'max_length[2]',
				'showPast' => 'max_length[8]',
				//genres remain unchecked
			];

			if (!$this->validate($rules)){
				$data['validation'] = $this->validator;
				$result = $festivalModel->getComingFestivals("","","","", NULL);
				$data['festivals'] = $result['festivals'];
				$data['genres'] = $result['genres'];
				$data['cmd'] = $result['cmd'];
			}
			else
			{
				$data['filters'] = [
					"numMonth" => $this->request->getPost('numMonth'),
					"showPast" => $this->request->getPost('showPast'),
					"type" => $this->request->getPost('type'),
					"camping" => $this->request->getPost('camping'),
					"genres" => $this->request->getPost('genres'),
				];
				$result = $festivalModel->getComingFestivals($this->request->getPost('numMonth'), $this->request->getPost('showPast'), $this->request->getPost('type'), $this->request->getPost('camping'), $this->request->getPost('genres'));
				$data['festivals'] = $result['festivals'];
				$data['genres'] = $result['genres'];
				$data['cmd'] = $result['cmd'];
			}
			
		}
		else
		{
			$result = $festivalModel->getComingFestivals("","","","", NULL);
			$data['festivals'] = $result['festivals'];
			$data['genres'] = $result['genres'];
			$data['cmd'] = $result['cmd'];
		}

		echo view('templates/header');
		echo view('overview_map', $data);
		echo view('overview', $data);
		echo view('templates/footer');
	}
}
