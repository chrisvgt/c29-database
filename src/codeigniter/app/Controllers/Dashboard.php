<?php namespace App\Controllers;

use App\Models\DashboardModel;
use App\Models\ReviewModel;
use App\Models\FestivalModel;

class Dashboard extends BaseController
{
	public function index()
	{
		$dashboardModel = new DashboardModel();
		$reviewModel = new ReviewModel();
		$festivalModel = new FestivalModel();

		$data = [
			'tickets' => $dashboardModel->getTicketsForLoggedInUser(),
			'reviews' => $reviewModel->getReviewsForLoggedInUser(),
		];


		foreach($data['reviews'] as &$review){
            $review['rating'] = [
                'festival' => $festivalModel->getRatingDisplay(round($review['festivalRating'] * 2) / 2),
                'location' => $festivalModel->getRatingDisplay(round($review['locationRating'] * 2) / 2),
            ];
        }
        unset($review);

		echo view('templates/header', $data);
		echo view('dashboard', $data);
		echo view('templates/footer');
	}

	//--------------------------------------------------------------------

}
