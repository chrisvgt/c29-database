<?php namespace App\Controllers;

use App\Models\FestivalModel;
use App\Models\TicketModel;
use App\Models\ReviewModel;

class Festival extends BaseController
{
	public function index()
	{
        $data = [];
        helper(['form']);
		$festivalModel = new FestivalModel();

        if (is_numeric($this->request->getVar('id')))
        {
            echo view('templates/header');
            echo view('festival_map', $festivalModel->getFestival($this->request->getVar('id')));
            echo view('festival', $festivalModel->getFestival($this->request->getVar('id')));
            echo view('templates/footer');
        }
        else
        {
            $data['validation'] = 'Invalid Request: Wrong ID';
			$result = $festivalModel->getComingFestivals("","","","", NULL);
			$data['festivals'] = $result['festivals'];
			$data['genres'] = $result['genres'];

            //sure, this shouldn't be a redirect? maybe with an error message passed as flash data
            echo view('templates/header');
            echo view('festival_map', $data);
            echo view('overview', $data);
            echo view('templates/footer');
        }
    }
    
    public function buytickets()
    {
        session()->setFlashdata('ticketTabSelected', 'active'); //after the redirect, the same tab will be selected

        $data = [];
        $db = db_connect();
        helper(['form']);

        if(!session()->get('isLoggedIn'))
        {
            return redirect()->to('/login');
        }

        if ($this->request->getMethod() == 'post')
        {
            /*
            example: 
            start transaction;
            insert into tickets (userID, templateID, `status`)
            values (1, 5, 'valid');
            update ticket_templates
            set soldTickets = soldTickets + 1
            where id = 5;
            commit;
            */

            $ticketData = [
                'userID' => session()->get('id'),
                'templateID' => $this->request->getPost('ttID')
            ];

            //executes the stored procedure "buyTicket()", passing the ticketTemplateID and userID and saving a boolean whether the ticket purchase was successful to a GLOBAL VARIABLE "ticketPurchaseSuccess" in the SQL Database, that can be accessed in a second query
            $c = "call buyTicket(" . $ticketData['templateID'] . ", " . $ticketData['userID'] . ", " . "@ticketPurchaseSuccess);";
            $db->query($c);
            $success = $db->query("select @ticketPurchaseSuccess;")->getResult();

            //success state table:
            // success is                               | when
            // -----------------------------------------------
            // null                                     | when this function has not been invoked
            // [{"@ticketPurchaseSuccess":null}]        | query fails due to error
            // [{"@ticketPurchaseSuccess":"0"}]         | transaction (purchase) failed
            // [{"@ticketPurchaseSuccess":"1"}]         | transaction (purchase) was successful

            //to send confirmation to then be displayed on the webpage https://www.webomelette.com/codeigniter-flashdata-session-data-with-redirect
            if($success !== NULL){
                if($success[0]->{'@ticketPurchaseSuccess'} === "1"){
                    session()->setFlashdata('purchaseConfirmation', 'Ticket purchase successful!');
                }else{
                    session()->setFlashdata('purchaseConfirmation', 'Ticket purchase failed!');
                }
            }

            return redirect()->to('/festival?id=' . $this->request->getPost('fID'));
        }
        else 
        {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    }

    public function writereview()
    {
        session()->setFlashdata('reviewTabSelected', 'active'); //after the redirect, the same tab will be selected

        $data = [];
        $db = db_connect();
        helper(['form']);

        if(!session()->get('isLoggedIn'))
        {
            return redirect()->to('/login');
        }

        if ($this->request->getMethod() == 'post'){
            
            if (!is_numeric($this->request->getPost('id')))
            {
                return redirect()->to('/');
            }

			$rules =
			[
                'id' => 'required|max_length[5]',
				'festivalRating' => 'required|max_length[1]',
				'locationRating' => 'required|max_length[1]',
				'reviewText' => 'max_length[1000]',
            ];
            
			if (!$this->validate($rules))
			{
                $data['validation'] = $this->validator;
                echo view('templates/header');
                echo view('overview', $data);
                echo view('templates/footer');
			}
			else
			{
                $reviewData = [
                    'userID' => session()->get('id'),
                    'festivalID' => $this->request->getPost('id'),
                    'festivalRating' => $this->request->getPost('festivalRating'),
                    'locationRating' => $this->request->getPost('locationRating'),
                    'reviewText' => ($this->request->getPost('reviewText') === NULL || $this->request->getPost('reviewText') === "") ? 'null' : ('"' . $this->request->getPost('reviewText') . '"')
                ];

                $c = "call writeReview(" . $reviewData['festivalID'] . ", " . $reviewData['userID'] . ", " . $reviewData['festivalRating'] . ", " . $reviewData['locationRating'] . ", " . $reviewData['reviewText'] . ", " . "@writeReviewSuccess);";
                $db->query($c);
                $success = $db->query("select @writeReviewSuccess;")->getResult();

                //to send confirmation to then be displayed on the webpage https://www.webomelette.com/codeigniter-flashdata-session-data-with-redirect
                if($success !== NULL){
                    if($success[0]->{'@writeReviewSuccess'} > 0){
                        session()->setFlashdata('reviewPostConfirmation', 'Review posted successfully!');
                    }elseif($success[0]->{'@writeReviewSuccess'} == 0){
                        session()->setFlashdata('reviewPostConfirmation', 'Posting review failed! Database error! We are very sorry!');
                    }elseif($success[0]->{'@writeReviewSuccess'} == -1){
                        session()->setFlashdata('reviewPostConfirmation', 'Posting review failed! The festival did not start yet. Wait until you get there to form an opinion!');
                    }elseif($success[0]->{'@writeReviewSuccess'} == -2){
                        session()->setFlashdata('reviewPostConfirmation', 'Posting review failed! You do not own a ticket to that festival. How can you rate a festival you have not attended?');
                    }elseif($success[0]->{'@writeReviewSuccess'} == -3){
                        session()->setFlashdata('reviewPostConfirmation', 'Posting review failed! None of your tickets have been voided. You apparently did not check-in at the festival correctly. How are we supposed to know if you even went? Get your ticket voided before posting a review!');
                    }elseif($success[0]->{'@writeReviewSuccess'} == -4){
                        session()->setFlashdata('reviewPostConfirmation', 'Posting review failed! You have already posted a review. You can only rate a festival once!');
                    }else{
                        session()->setFlashdata('reviewPostConfirmation', 'Unknown error! We are very sorry!');
                    }
                }

                return redirect()->to('/festival?id='.$this->request->getPost('id'));
            }
        }
        else {
            throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
        }
    } 
}
