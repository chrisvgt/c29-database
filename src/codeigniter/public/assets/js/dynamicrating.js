var locationRating = 0;
var festivalRating = 0;

$(document).on("click", ".dynamicRating", function (event) {

    var star = jQuery(this);
    var rating = star.attr("value");
    var name = star.parent().attr("id");

    if(name == "locationRating"){
        locationRating = rating;
    }else{
        festivalRating = rating;
    }

    jQuery(star).removeClass("far").addClass("fas").prevUntil("label").removeClass("far").addClass("fas");
    jQuery(star).nextAll().removeClass("fas").addClass("far");

    //console.log(name, rating);
});

function addPostData(){
    var form = document.forms["reviewForm"];

    if(locationRating == 0 || festivalRating == 0){
        alert("you need to rate the festival");
        return false;
    }

    form.locationRating.value = locationRating;
    form.festivalRating.value = festivalRating;
    return true;
};