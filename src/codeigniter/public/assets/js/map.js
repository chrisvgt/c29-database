//params:
// * festivals: array of festival objects to be shown on map. each festival object needs to have at least two members called 'latitude' and 'longitude'.
// * center: center of map. if no center is provided, the map will be eurocentric (centered around germany)
// * makeMarkersClickable: if true, clicking on a marker will redirect to its festival. for this, each object in festivals needs to have an member 'id' containing the festival id
//note regarding "lat and lng" vs "latitude and longitude": the former will be used with the api and have to be floats; these names should only appear as javascript entities. the latter will be used for interfacing the database / php code and should not be used as javascript entities; they are usualy formatted as strings.
function initMap(festivals, center = null, zoom = null, makeMarkersClickable = false) {
    let mapCenter; //mapCenter is expected to have the members lat and lng
    if(center !== null){
        mapCenter = { lat: parseFloat(center['latitude']), lng: parseFloat(center['longitude'])};
    }else{
        mapCenter = { lat: 50, lng: 10 }; //center on user?
    }

    if(zoom === null){ //set default zoom
        zoom = 2;
    }

    map = new google.maps.Map(document.getElementById("map"), { //create map
        center: mapCenter,
        zoom: zoom,
    });

    //marker style
    //marker design https://developers.google.com/maps/documentation/javascript/markers
    //cluster: https://developers.google.com/maps/documentation/javascript/marker-clustering#maps_marker_clustering-javascript
    let markerScale = 30;
    const markerIcon = {
        url:"https://raw.githubusercontent.com/twitter/twemoji/master/assets/72x72/1f389.png",
        scaledSize: new google.maps.Size(markerScale, markerScale), //square
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(0, markerScale),
    };

    let makers = festivals.map(function(festival){            
            let pos = {lat: parseFloat(festival['latitude']), lng: parseFloat(festival['longitude'])}; //pos has to have members called lat and lng
            
            //filter missing locations to not display them on the map
            if(!pos['lat'] && !pos['lng']){ //i assume that if one is missing, the other must be missing too. otherwise, the value is regarded as valid.
                return null;
            }

            newMarker = new google.maps.Marker({
                position: pos,
                icon: markerIcon,
                map: map,
                url: 'festival?id=' + festival['id']
            });

            if(makeMarkersClickable === true){
                google.maps.event.addListener(newMarker, 'click', function () {
                    window.location.href = this.url;
                });    
            }
            
            return newMarker;
        }
    );
}