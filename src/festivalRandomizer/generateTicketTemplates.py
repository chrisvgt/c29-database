import random
import csv
from datetime import datetime
from datetime import timedelta
from datetime import time
import CSVHelper

#this script randomizes a few ticket templates for every festival in the source file

# settings
SOURCE_FILEPATH = "festival.csv"
OUTPUT_FILEPATH = "ticketTemplates.csv"
CSV_DELIMITER = ","
NORMAL_TICKET_TEMPLATE_NAME = "Base Package"
DEFAULT_SOLD_TICKETS = 0
#totalTickets = base_number * 10 ^ scaling = random(minTotalTickets, maxTotalTickets) * 10 ^ random(minScaling, maxScaling)
DEFAULT_MIN_TOTAL_TICKETS = 10
DEFAULT_MAX_TOTAL_TICKETS = 99
DEFAULT_MIN_SCALING = 1
DEFAULT_MAX_SCALING = 2
#min/max number of additional templates to generate for this festival 
MIN_TEMPLATES = 1
MAX_TEMPLATES = 3
#start time
MIN_START_TIME = 9
MAX_START_TIME = 11
#end time
MIN_END_TIME = 21
MAX_END_TIME = 23
#ticket options
#price is the price multiplier per Day, thus, the final ticket price is: price multiplier(per Day) * (basePrice / # of days for this festival) * # of days for this ticket
#if a starttime or endtime is specified, that time needs to be used
#tags give meta info:
# random-number-of-days: this ticket is only valid for a randomly generated number of days < total number of days and can only be used for multi day festivals
SPECIAL_TICKETS = [
                            {
                                "name": "PARTY-HARD PACKAGE",
                                "price": 1.15,
                            },
                            {
                                "name": "VIP PACKAGE",
                                "price": 1.50,
                            },
                            {
                                "name": "COMFORT+ PACKAGE",
                                "price": 1.30,
                            },
                            {
                                "name": "STAY HYDRATED PACKAGE",
                                "price": 1.50,
                            },
                            {
                                "name": "AFTERNOON PACKAGE",
                                "price": 0.70,
                                "startTime": 15,
                                "tags": ["one-day"]
                            },
                            {
                                "name": "LURKING PACKAGE",
                                "price": 1.10,
                                "tags": ["random-number-of-days"],
                            },
                        ]

#vars
fr = CSVHelper.CSVHelper(SOURCE_FILEPATH)
templateID = 1 # MySQL index starts by 1

with open(SOURCE_FILEPATH, newline='') as csvfile:
    f = csv.reader(csvfile, delimiter = CSV_DELIMITER)

    #find in what row, what data is stored in the festival csv
    priceColumn = fr.findColumnForData("price")
    idColumn = fr.findColumnForData("id")
    startDateColumn = fr.findColumnForData("startDate")
    endDateColumn = fr.findColumnForData("endDate")
    
    #open output file
    with open(OUTPUT_FILEPATH, 'w', newline='') as outputFile:
        ticketTemplateFieldnames = ["templateID", "festivalID", "price", "name", "validFromDate", "expirationDate", "totalTickets", "soldTickets"]
        ticketTemplateWriter = csv.DictWriter(outputFile, delimiter = CSV_DELIMITER, fieldnames = ticketTemplateFieldnames)
        ticketTemplateWriter.writeheader()

        #generate ticket templates for all festivals and save them
        for i, row in enumerate(f):
            #extract known data from festival csv
            id = row[idColumn]
            price = row[priceColumn]
            startDate = row[startDateColumn]
            endDate = row[endDateColumn]

            if i == 0:
                #header
                pass
            else:
                random.seed(str(id) + str(price) + str(startDate) + str(endDate)) #seeding with festivalspecific info
                numberOfTemplates = random.randint(MIN_TEMPLATES, MAX_TEMPLATES)

                #generate default template start and end time
                startDate = datetime.strptime(startDate + " " + str(random.randint(MIN_START_TIME, MAX_START_TIME)).zfill(2) + ":00:00", "%Y-%m-%d %H:%M:%S")
                endDate = datetime.strptime(endDate + " " + str(random.randint(MIN_END_TIME, MAX_END_TIME)).zfill(2) + ":00:00", "%Y-%m-%d %H:%M:%S")

                totalDuration = (endDate - startDate).days + 1 #number of days on which the festival takes place

                #default template
                templates = []
                templates.append(
                    {
                        "templateID": templateID,
                        "festivalID": id,
                        "price": price,
                        "name": NORMAL_TICKET_TEMPLATE_NAME,
                        #date_time_obj += timedelta(days=random.randint(MIN_DATE, MAX_DATE)) # randomly increment days
                        "validFromDate": startDate,
                        "expirationDate": endDate,
                        "totalTickets": random.randint(DEFAULT_MIN_TOTAL_TICKETS, DEFAULT_MAX_TOTAL_TICKETS) * 10 ** random.randint(DEFAULT_MIN_SCALING, DEFAULT_MAX_SCALING),
                        "soldTickets": DEFAULT_SOLD_TICKETS,
                    }
                )
                templateID += 1

                #generate additional templates for this festival
                allowedTemplates = SPECIAL_TICKETS.copy() #resets for each festival, names that haven't been used in this festival
                if totalDuration <= 1: #remove all tickets that go at least one day for one-day-festivals
                    for at in allowedTemplates:
                        if 'tags' in at and "random-number-of-days" in at['tags']:
                            allowedTemplates.remove(at)
                        
                for t in range(numberOfTemplates):
                    if not allowedTemplates:
                        print("error: there are no names left to give to ticket templates. please add more names or reduce the number of ticket templates to be generated!")
                        exit()

                    choice = random.choice(allowedTemplates)

                    if totalDuration > 1 and 'tags' in choice and "random-number-of-days" in choice['tags']:
                        ticketDuration = random.randint(0, totalDuration - 2)
                        validFromDate = startDate + timedelta(days = random.randint(0, totalDuration - ticketDuration - 1))
                        expirationDate = validFromDate + timedelta(days = ticketDuration)
                        expirationDate = expirationDate.replace(hour = endDate.hour)
                        #print(f"generating lurker with festivalID:{id}, totalDuration:{totalDuration}, ticketDuration:{ticketDuration}, startoffset:{(validFromDate - startDate).days - 1} startDate:{startDate}, validFromDate:{validFromDate}, endDate:{endDate}, expirationDate{expirationDate}") #debug to validate, this is working
                    elif totalDuration > 1:
                        ticketDuration = totalDuration
                        validFromDate = startDate
                        expirationDate = endDate
                    else: #one-day festival
                        ticketDuration = 1
                        totalDuration = 1
                        validFromDate = startDate
                        expirationDate = endDate

                    if 'startTime' in choice:
                        validFromDate = validFromDate.replace(hour = choice['startTime'])
                    
                    if 'endTime' in choice:
                        expirationDate = expirationDate.replace(hour = choice['endTime'])

                    if 'tags' in choice and "one-day" in choice['tags']:
                        expirationDate = expirationDate.replace(year = validFromDate.year, month = validFromDate.month, day = validFromDate.day)
                    
                    if startDate > validFromDate or endDate < expirationDate: #may never be the case
                        print(f"error: festivalID:{id}: startDate > validFromDate ({startDate > validFromDate}) and endDate < expirationDate ({endDate < expirationDate})")

                    if 'tags' in choice and "random-number-of-days" in choice['tags'] and startDate.date() == validFromDate.date() and endDate.date() == expirationDate.date(): #special test for random number of days for a ticket (needs to be smaller than the total duration)
                        print(f"error: festivalID:{id}: startDate.date() {startDate.date()} == validFromDate.date() {endDate.date()} ({startDate.date() == validFromDate.date()}) and endDate.date() == expirationDate.date() ({endDate.date() == expirationDate.date()})")


                    templates.append(
                        {
                            "templateID": templateID,
                            "festivalID": id,
                            "name": choice['name'],
                            "validFromDate": validFromDate,
                            "expirationDate": expirationDate,
                            "price": round(choice['price'] * (float(price) / totalDuration) * (ticketDuration + 1), 2),
                            "totalTickets": round(random.randint(DEFAULT_MIN_TOTAL_TICKETS * 10 ** DEFAULT_MIN_SCALING, templates[0]['totalTickets']), -1),
                            "soldTickets": DEFAULT_SOLD_TICKETS,
                        }
                    )
                    templateID += 1

                    allowedTemplates.remove(choice) #the name that was just used cannot be used again

                #YYYY-MM-DD hh:mm:ss

                #generate eastereggs

                #debug display
                for template in templates:
                #    print("************************")
                #    for k, v in template.items():
                #        print(f"{k}: {v}")
                    ticketTemplateWriter.writerow(template)