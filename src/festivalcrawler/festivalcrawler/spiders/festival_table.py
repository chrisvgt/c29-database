import scrapy
import random
from datetime import datetime
from datetime import timedelta
from ..items import FestivalcrawlerItem

### CONSTANTS ###
# GENERATOR
#settings for ticket prices, min and max in euro
MIN_PRICE = 25
MAX_PRICE = 150
#settings for date
MIN_DAYS = 2
MAX_DAYS = 10

def generatePrice(name, location, genre):
    PRICE_SEPERATOR = "." #char that seperates euro from cents. should be comma or dot.
    #festival specific values are used to seed the random number generators
    if name is None:
        name = ""
    if location is None:
        location = ""
    if genre is None:
        genre = ""
        
    random.seed(name + genre)
    euro = random.randint(MIN_PRICE, MAX_PRICE)
    random.seed(location + genre)
    cent = random.randint(0, 99)
    return str(euro)  + PRICE_SEPERATOR +  str(cent)

class FestivalTableSpider(scrapy.Spider):
    name = 'festival_table'
    allowed_domains = ['festival-alarm.com']
    #member settings
    festivalCounter = 1 # MySQL Autoincrement starts by 1
    START_YEAR = 2014
    END_YEAR = 2021
    SHOW_NUMBER_CRAWLED_FESTIVALS = False #shows debug output of the number of crawled festivals
    NONE_VALUE_IN_OUTPUT = r"\N" #this is what is output, if some data hasn't been found / is None, leave blank for "" in output
                                 #MySQL's LOAD DATA tool interprets \N as being a NULL value

    def start_requests(self):
        for y in range(self.START_YEAR, self.END_YEAR + 1):
            self.year = str(y)
            yield scrapy.Request('https://www.festival-alarm.com/Festivals-' + self.year, meta={'year':self.year})

    def parse(self, response):
        for row in response.css("table tbody tr"):
            item = FestivalcrawlerItem() #item that will later be passed to the subpage (where additional content can be parsed and then all the data is returned together)
            item.setNone() #init all fields as None

            #parse data from the row
            #apparently the "is none" check only works before inserting into item, thus it is done here
            item['name'] = row.css("td.event-title a::text").get()
            item['genre']  = row.css('td.event-genre::text').get()
            item['country'] = row.css('div.address [itemprop="addressCountry"]::text').get()
            item['zipcode'] = row.css('div.address [itemprop="postalCode"]::text').get()
            item['city'] = row.css('div.address [itemprop="addressLocality"]::text').get()
            item['location'] = row.css('td.event-venue [itemprop="name"]::text').get()
            item['startDate'] = row.css('[itemprop="startDate"]::text').get()
            item['endDate'] = row.css('[itemprop="endDate"]::text').get()

            #generating price if no price was found
            priceNumber = row.css('td.ticket_price::attr(data-sort)').get()
            priceString = row.css('td.ticket_price::text').get()

            if priceString is not None:
                priceString = priceString.strip()
            if priceNumber is not None: #price number is also a string (that needs to be stripped to assure proper processing afterwards)
                priceNumber = priceNumber.strip()
            if priceNumber == "0" and priceString == "n.v.":
                priceNumber = generatePrice(item['name'], item['location'], item['genre'])
                print(f"generated price for festival {item['name']}: price = {priceNumber}")
            elif (priceNumber == "0") != (priceString == "n.v."): #if priceNumber is zero and priceString is not or the other way around, something is probably wrong, this behaviour should be investigated and because of uncertainty of what is going on, is price will be generated
                print(f"WARNING: festival {item['name']}: priceNumber: {priceNumber} but priceString: {priceString} => something is wrong here!")
                priceNumber = generatePrice(item['name'], item['location'], item['genre'])
                print(f"generated price for festival {item['name']}: price = {priceNumber}")
            item['price'] = priceNumber

            #get link to festival page and follow, parsing the festival page aswell.
            #item is passed as metadata so more data can be added from the subpage in parse_festival(). from parse_festival() item is returned and that value (item) is then again returned from parse()
            subpage = row.css("td.event-title a::attr(href)").get()
            rec = response.follow(subpage, callback = self.parse_festival, meta={'festivalItem':item, 'festivalYear':response.meta['year']}) #pass item with already collected data as metadata
            yield rec

    def parse_festival(self, response):
        #retrieve passed item
        item = response.meta['festivalItem']
        festivalYear = response.meta['festivalYear']

        #parse additional info from subpage
        #take the link where the image source is from the google maps api
        if response.css("div.festival_full div.range div.cell-sm-12 img[src^='https://maps.googleapis.com/maps/api/staticmap']::attr(src)").get() is not None:
            # parse google maps link to coordinates, could be fancier :)
            tmp = response.css("div.festival_full div.range div.cell-sm-12 img[src^='https://maps.googleapis.com/maps/api/staticmap']::attr(src)").get().strip()
            START = '|color:red|'
            END = '&key'
            tmp = tmp[tmp.find(START)+len(START):tmp.find(END)]
            tmp = tmp.split(',')
            
            # select data correct data to item
            item['lat'] = tmp[0]
            item['lng'] = tmp[1]
        else:
            item['lat'] = r"\N" #MySQL's LOAD DATA tool interprets \N as being a NULL value
            item['lng'] = r"\N" #MySQL's LOAD DATA tool interprets \N as being a NULL value

        infoBoxList = response.css("div.festival-info-box li span ::text").getall() #1. get all info boxes 2. find the one containing camping and indoor/outside info 3. extract
        for elem in infoBoxList:
            text = elem.strip() #in case of accidental whitespaces
            if text.startswith("Wo"): #search for indoor/outdoor
                if "draußen" in text:
                    item['type'] = "outdoor"
                elif "drinnen" in text:
                    item['type'] = "indoor"
                else : 
                    item['type'] = r"\N" #MySQL's LOAD DATA tool interprets \N as being a NULL value
            elif text.startswith("Camping"): #search for camping
                if "ja" in text:
                    item['withCamping'] = 1
                elif "nein" in text:
                    item['withCamping'] = 0
                else : 
                    item['withCamping'] = r"\N" #MySQL's LOAD DATA tool interprets \N as being a NULL value

        #strip, warn for missing data and cleanup (e.g. adding year to date)
        for key in item:
            if type(item[key]) == str:
                item[key] = item[key].replace('\n', '').strip() 
                if item[key] is not None:
                    # DATE CONVERSION, mysql datetime format: YYYY-MM-DD hh:mm:ss
                    # and add year to the dates
                    if key == 'startDate' or key == 'endDate':
                            item[key] = item[key] + festivalYear
                            tmp = str(item[key])
                            # string to datetime
                            tmp = tmp.replace('.','-') # convert dot to readable seperator
                            date_time_obj = datetime.strptime(tmp,'%d-%m-%Y')
                            # datetime to string
                            tmp = str(date_time_obj.strftime('%Y-%m-%d'))
                            item[key] = tmp

            #generate a start date, if it doesn't exist
            if item['startDate'] is None:
                print(f"generate start date for festival {item['name']}")
                tmp = str(random.randint(1, 28)) + "-" + str(random.randint(1, 12)) + "-" + festivalYear #day is compatible with the shortest possible month (feburary which might only have 28 days)
                date_time_obj = datetime.strptime(tmp,'%d-%m-%Y')
                tmp = str(date_time_obj.strftime('%Y-%m-%d'))
                item['startDate'] = tmp
                oneDayFestival = False #means, this festival lasts longer than one day
            elif item['endDate'] is None:
                oneDayFestival = True #if a start date exists, and no end date exists, we know that this festival only lasts one day
            else:
                oneDayFestival = False #if a start and an end date exists, this variable is pretty much meaning less, but should be False, to avoid setting endDate = startDate

            # set endDate if empty
            if oneDayFestival: #if the start date was set and the and date was not, it is a one day festival
                print(f"one day festival {item['name']}")
                item['endDate'] = item['startDate']
            elif item['endDate'] is None and item['startDate'] is not None: #if the startDate had to be generated, generate an end date
                print(f"generate end date for festival {item['name']}")
                tmp = item['startDate'] 
                # string to datetime
                date_time_obj = datetime.strptime(tmp,'%Y-%m-%d')
                date_time_obj += timedelta(days=random.randint(MIN_DAYS, MAX_DAYS)) # randomly increment days
                # datetime to string
                tmp = str(date_time_obj.strftime('%d-%m-%Y'))
                item['endDate'] = tmp

            # items which are none set to ""
            if item[key] is None:
                item[key] = self.NONE_VALUE_IN_OUTPUT
                print(f"WARNING: no data found for {key} for festival {item['name']}")

        #counting crawled festivals
        item['id'] = self.festivalCounter
        self.festivalCounter = self.festivalCounter + 1
        if self.SHOW_NUMBER_CRAWLED_FESTIVALS:
            print(f"numberOfFestivals crawled: {self.festivalCounter}")

        #return item that now contains even more data
        yield item