# Define here the models for your scraped items
#
# See documentation in:
# https://docs.scrapy.org/en/latest/topics/items.html

import scrapy

class FestivalcrawlerItem(scrapy.Item):
    #from table
    id = scrapy.Field()
    name = scrapy.Field()
    price = scrapy.Field()
    genre = scrapy.Field()
    country = scrapy.Field()
    zipcode = scrapy.Field()
    city = scrapy.Field()
    location = scrapy.Field()
    startDate = scrapy.Field()
    endDate = scrapy.Field()

    #from subpage
    lng = scrapy.Field()
    lat = scrapy.Field()
    type = scrapy.Field()
    withCamping = scrapy.Field()

    def setNone(self):
        self['id'] = None
        self['name'] = None
        self['price'] = None
        self['genre'] = None
        self['country'] = None
        self['zipcode'] = None
        self['city'] = None
        self['location'] = None
        self['startDate'] = None
        self['endDate'] = None
        self['lng'] = None
        self['lat'] = None
        self['type'] = None
        self['withCamping'] = None