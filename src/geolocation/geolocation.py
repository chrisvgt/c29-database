
import requests
import csv
import CSVHelper
from operator import itemgetter

#settings
FILEPATH = "festival.csv"
LOCATIONS_OUTPUT_FILEPATH = "locations.csv"
LOCATIONS_AND_FESTIVALS_OUTPUT_FILEPATH = "locationsAndFestivals.csv"
CSV_DELIMITER = ","
API_KEY_FILEPATH = "googleAPIKey" #this file contains the google maps api key in the first line (nothing else), that is required to use the api
NONE_NULL_VALUE_REPLACEMENT = "\\N"

#read api key
with open(API_KEY_FILEPATH, "r") as gapikeyfile:
    key = gapikeyfile.readline().strip()

import os
os.environ["GOOGLE_API_KEY"] = key
import geocoder

#instantiate csv file reader
fr = CSVHelper.CSVHelper(FILEPATH)

#read csv
with open(FILEPATH, newline='') as csvfile:
    f = csv.reader(csvfile, delimiter = CSV_DELIMITER)

    #find in what row, what data is stored in the csv
    col = {}
    col['id'] = fr.findColumnForData("id")
    col['lat'] = fr.findColumnForData("lat")
    col['lng'] = fr.findColumnForData("lng")
    col['location'] = fr.findColumnForData("location")
    col['websiteZipcode'] = fr.findColumnForData("zipcode")
    col['websiteCity'] = fr.findColumnForData("city")
    col['websiteCountryCode'] = fr.findColumnForData("country")

    #make sure all columns were found
    for k, v in col.items():
        if v is None:
            print(f"error: {k} could not be found as a column in the given csv file!")
            exit

    #create data structure, that will hold the final data
    #dict with keys beeing tuples of (lat, lng) and values beeing dicts of keys beeing locationnames and values beeing location data including a list of festivalids
    #d = {
    #    (lat, lng): {
    #        name: {
    #            festivalIDList: [festivalID],
    #            countryCode
    #            zipcode
    #            city
    #            street
    #            street_number
    #        },
    #    },
    #}

    d = {}
    #each line in festival csv
    for i, row in enumerate(f):
        if i > 0: #content of csv
            festivalID = int(row[col['id']])
            name = row[col['location']]
            try:
                coords = (float(row[col['lat']]), float(row[col['lng']]))
            except ValueError:
                coords = (53.5423712, 10.02268659) #all festivals, where coords are missing now take place in the Hamburger Großmarkt (as an easy fix)
                name = "Hamburger Großmarkt"
                print(f"cannot convert coords for festival {festivalID}, they are probably missing...selecting '53.5423712,10.0226865' (Hamburger Großmarkt)")

            

            #we believe, that if the name and coordinates of a location are the same, the location is the same 
            #this code guarantees that d in the end only contains name + coordinate combinations, that are unique. and that each of these combinations kept its association with the corresponding festival.
            if coords in d and name in d[coords]: #that particular entry (name and coords) are 
                d[coords][name]['festivalIDList'].append(festivalID)
            elif coords not in d: #create new entry, either inserting only name, if coords exist, or inserting name and coords
                d[coords] = {
                    name: {
                        'festivalIDList': [festivalID],
                        'countryCode': None,
                        'zipcode': None,
                        'city': None,
                        'street': None,
                        'street_number': None,
                    }
                }
            else: #coordinates are in d, but name is not in d[coords]
                d[coords][name] = {
                    'festivalIDList': [festivalID],
                    'countryCode': None,
                    'zipcode': None,
                    'city': None,
                    'street': None,
                    'street_number': None,
                }

    #go through entire dict and do reverse and forward request for all coords, insert response into all name fields. then ive got all addresses for all locations.
    #with requests.Session() as session:
    for k in d.keys(): # k = tuple of coordinates, key
        print(f"location at {k}: sending request 1/2 (reverse)...")
        rev = geocoder.google(k, method='reverse', key = key)
        print(f"location at {k}: sending request 2/2 (forward)...")
        res = geocoder.google(rev.address, key = key)

        for n in d[k].keys(): # n = location name, key
            d[k][n]['countryCode'] = res.country
            d[k][n]['zipcode'] = res.postal
            d[k][n]['city'] = res.city_long
            d[k][n]['street'] = res.street_long
            d[k][n]['street_number'] = res.housenumber
            
            for attributeName, attributeValue in d[k][n].items(): #replace all none values with the value of NONE_NULL_VALUE_REPLACEMENT to allow for "\n" or similar for easier database import
                if attributeValue is None:
                    d[k][n][attributeName] = NONE_NULL_VALUE_REPLACEMENT

    #then i need to build two csvs:
    #1) a location csv that contains location data (id, name, city, zipcode, street, streetNumber, lng, lat)
    #2) a festivalAndLocations csv that holds all festival ids *in ascending order* each with one corresponding location id.

    locationIDCounter = 1 # MySQL index starts by 1
    with open(LOCATIONS_OUTPUT_FILEPATH, 'w', newline = '') as locationsOutFile, open(LOCATIONS_AND_FESTIVALS_OUTPUT_FILEPATH, 'w', newline = '') as locationsAndFestivalsOutFile:
        locationsFieldnames = ["locationId", "latitude", "longitude", "locationName", "countryCode", "zipcode", "city", "street", "street_number"]
        locationsWriter = csv.writer(locationsOutFile, delimiter = CSV_DELIMITER)
        locationsWriter.writerow(locationsFieldnames)
        
        locationsAndFestivalsFieldnames = ["festivalID", "locationID"]
        locationsAndFestivalsWriter =  csv.writer(locationsAndFestivalsOutFile, delimiter = CSV_DELIMITER)
        locationsAndFestivalsWriter.writerow(locationsAndFestivalsFieldnames)

        locationsAndFestivalsList = []
        
        for k in d.keys(): # k = tuple of coordinates, key
            for n in d[k]: # n = location name, key
                locationsWriter.writerow([locationIDCounter, k[0], k[1], n, d[k][n]['countryCode'], d[k][n]['zipcode'], d[k][n]['city'], d[k][n]['street'], d[k][n]['street_number']])
                for fID in d[k][n]['festivalIDList']:
                    locationsAndFestivalsList.append((fID, locationIDCounter)) #fill list, so i can write the locationsAndFestivals file later in ascending order based on festivalID
                locationIDCounter += 1

        #first run created / fill locationsAndFestivalsList, now sort and print
        locationsAndFestivalsList = sorted(locationsAndFestivalsList, key=itemgetter(0))
        locationsAndFestivalsWriter.writerows(locationsAndFestivalsList)