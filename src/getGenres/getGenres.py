import csv
import CSVHelper

#this script creates
# genres.csv: containing a list with unique entries of all genres in all festivals in the input file for the "genres" table
# genresAndFestivals.csv: containing a list with unique entries of every festival with every corresponding genre (festivalID + genre) for the "genresAndFestivals" table

# settings
FILEPATH = "test.csv"
FILEPATH_OUTPUT_GENRES = "genres.csv"
FILEPATH_OUTPUT_GENRES_AND_FESTIVALS = "genresAndFestivals.csv"
CSV_DELIMITER = ","

id = 'id'
genre = 'genre'

genreList = [] #will contain a unique list of all genres in the end of the program, content for "genres" table
genresAndFestivalsList = [] #will contain a list of sublists associating every festival with every genre this festival has

#there are a few probably very unnessecary data type conversions (from list to set to dict to list of dicts etc.), that could probably be nicer, but its working for now

# load where in the festival file the genres are stored
#read csv
fr = CSVHelper.CSVHelper(FILEPATH)
with open(FILEPATH, newline='') as csvfile:
    f = csv.reader(csvfile, delimiter = CSV_DELIMITER)

    col = {}
    col[id] = fr.findColumnForData(id)
    col[genre] = fr.findColumnForData(genre)

    #make sure all columns were found
    for k, v in col.items():
        if v is None:
            print(f"error: {k} could not be found as a column in the given csv file!")
            exit
        else:
            print(f"col['{k}'] is {v}")

    #parse each line in csv
    for i, row in enumerate(f):
        data = {}
        if i > 0: #content of csv
            #get data
            data[id] = row[col[id]]
            data[genre] = row[col[genre]]

            #check if data is complete, genre also can't be an empty string bc then it would be None
            for k, v in data.items():
                if v is None:
                    print(f"warning: {k} is None in festival {data[i]}, line {i}")

            print(f"festival #{data[id]}: {data[genre]}")

            data[genre] = data[genre].split(",") #data[genre] now contains a list of all genres
            for j, elem in enumerate(data[genre]): #strip all elements
                data[genre][j] = elem.strip()
                genresAndFestivalsList.append((data[id], elem.strip())) #tupel(festivalid, genre)

            genreList.extend(data[genre])

        else: #header
            pass

    #results
    genreList = list(set(genreList)) #make list unique
    print(genreList)
    genresAndFestivalsList = list(set(genresAndFestivalsList)) #make sure, just in case the source data is faulty, that every festival only has every genre associated once
    print(genresAndFestivalsList)

    #turn into dict for better writing
    for j, elem in enumerate(genreList): #genreList
        genreList[j] = {genre: genreList[j]}

    for j, elem in enumerate(genresAndFestivalsList):
        genresAndFestivalsList[j] = {'festival' + id: genresAndFestivalsList[j][0], genre: genresAndFestivalsList[j][1]}
        print(genresAndFestivalsList)

    #now put results into a genres.csv and genresAndFestivals.csv
    with open(FILEPATH_OUTPUT_GENRES, 'w', newline='') as genresOutput:
        genresFieldnames = [genre]
        genresWriter = csv.DictWriter(genresOutput, delimiter = CSV_DELIMITER, fieldnames = genresFieldnames)
        genresWriter.writeheader()
        for e in genreList:
            genresWriter.writerow(e)

    with open(FILEPATH_OUTPUT_GENRES_AND_FESTIVALS, 'w', newline='') as genresAndFestivalsOutput:
        genresAndFestivalsFieldnames = ['festival' + id, genre]
        genresAndFestivalsWriter = csv.DictWriter(genresAndFestivalsOutput, delimiter = CSV_DELIMITER, fieldnames = genresAndFestivalsFieldnames)
        genresAndFestivalsWriter.writeheader()
        for e in genresAndFestivalsList:
            genresAndFestivalsWriter.writerow(e)