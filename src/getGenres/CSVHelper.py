import random
import csv

class CSVHelper:
    file = None
    delimiter = None

    def __init__(self, csvFilepath, delimiter = ","):
        self.file = csvFilepath
        self.delimiter = delimiter

    def findColumnForData(self, columnName):
        location = None

        with open(self.file) as csvfile:
            f = csv.reader(csvfile, delimiter = self.delimiter) #file
            
            for row in f:
                #find location
                for i, col in enumerate(row):
                    if col == columnName:
                        location = i
                        break

                if location is None:
                    print(f"ERROR: columnName ({columnName}) can not be found! No further processing possible")
                    return

        return location
