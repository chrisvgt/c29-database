### IMPORT DATA FROM CSV FILES ###
use proj_festival;
SET foreign_key_checks = 0;

# IMPORT COUNTRIESDATA TABLE #
LOAD DATA INFILE '/var/lib/mysql-files/festival/countryCodes.csv'
INTO TABLE countries
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS
(`country` , `countryCode`);

# IMPORT FESTIVALDATA TABLE #
LOAD DATA INFILE '/var/lib/mysql-files/festival/festivalData.csv'
INTO TABLE festivals
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS
# city , country, endDate, genre, id, lat, lng, location, name , price , startdate, type, withCampin, zipcode
(@dummy , @dummy, endDate, @dummy, locationID ,@dummy, @dummy, @dummy, `name`, @dummy, `startDate`, `type`, `withCamping`, @dummy);

# IMPORT LOCATIONS TABLE #
LOAD DATA INFILE '/var/lib/mysql-files/festival/locations.csv'
INTO TABLE locations
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS
(`id`, `latitude`, `longitude`, `name`, `countryCode`, `zipcode`, `city`, `street`, `streetNumber`);

# IMPORT LOCATIONSANDFESTIVALS TABLE #
DROP TABLE IF EXISTS tmp_locationsAndFestivals;
CREATE TABLE tmp_locationsAndFestivals( # create tmp table to store data from csv
	festivalID int,
    locationID int
);

LOAD DATA INFILE '/var/lib/mysql-files/festival/locationsAndFestivals.csv'
INTO TABLE tmp_locationsAndFestivals
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS
(festivalID,locationID);

# update locationID from tmp table to festivals
UPDATE festivals t1, tmp_locationsAndFestivals t2 SET t1.locationID = t2.locationID + 1 WHERE t1.id = t2.festivalID;
DROP TABLE IF EXISTS tmp_locationsAndFestivals;

# IMPORT GENRESDATA TABLE #
LOAD DATA INFILE '/var/lib/mysql-files/festival/genres.csv'
INTO TABLE genres
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS
(name);

# IMPORT GENRESANDFESTIVALSDATA TABLE #
LOAD DATA INFILE '/var/lib/mysql-files/festival/genresAndFestivals.csv'
INTO TABLE genresAndFestivals
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS
(`festivalID`, `genre`);

# IMPORT TICKETTEMPLATES TABLE #
LOAD DATA INFILE '/var/lib/mysql-files/festival/ticketTemplates.csv'
INTO TABLE ticket_templates
FIELDS TERMINATED BY ',' 
ENCLOSED BY '"'
LINES TERMINATED BY '\r\n'
IGNORE 1 ROWS
(`id`, `festivalID`, `price`, `option`, `validFromDate`, `expirationDate`, `totalTickets`, `soldTickets`);

SET foreign_key_checks = 1;

# SAMPLE OUTPUT
SELECT * FROM festivals, genresAndFestivals WHERE festivals.id = genresAndFestivals.festivalID LIMIT 50;
SELECT * FROM proj_festival.festivals, locations WHERE locations.id = festivals.locationID LIMIT 50;