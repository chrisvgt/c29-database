drop procedure if exists buyTicket;
drop procedure if exists writeReview;

delimiter //
-- only allows buying tickets, if the ticket can still be used (i.e. the ticket is not exprired and if not all tickets are sold)
create procedure buyTicket(
    in tID int, -- template id
    in uID int, -- user id
    out success tinyint(1) -- true, if purchase was successfull, else false
)
begin
    declare s int unsigned; -- sold tickets 
    declare t int unsigned; -- total tickets 
    declare exDate datetime; -- expiration date
    declare n datetime; -- now timestamp
    
    start transaction;
        select `soldTickets`, `totalTickets`, `expirationDate`, now() into s, t, exDate, n from `ticket_templates` where `id` = tID;
        
        if(s < t and exDate > n) then -- there are still tickets to be sold and the ticket is not yet expired
            insert into tickets(`userID`, `templateID`, `status`) values (uID, tID, 'valid');
            update ticket_templates set soldTickets = soldTickets + 1 where id = tID;
            set success = true;
        else
            set success = false;
            rollback;
        end if;
    commit;
end//

-- inserts review data into reviews-table. the data is only inserted, if the festival has already started (bc otherwise, one would not have been at the event and therefore can't share an opinion).
-- also the user needs to hold at least one voided ticket, showing they attended the festival.
-- a success code is returned.
-- success = 0: some unknown error causing no rows to be inserted
-- success > 0: success (should be =1)
-- success = -1: festival has not started yet
-- success = -2: user has no tickets
-- success = -3: user has no voided tickets
-- success = -4: user has already written a review
create procedure writeReview(
    in fID int, -- festival id
    in uID int, -- user id
    in fR tinyint unsigned, -- festival rating
    in lR tinyint unsigned, -- location rating
    in t varchar(1000), -- review text
    out success tinyint -- true, if saving review was successfull, else false
)
begin
    declare s datetime; -- start date
    declare n datetime; -- now timestamp
    declare hasTickets int; -- holds the number of tickets the current user has for the current festival.
    declare hasVoidedTickets int; -- holds the number of tickets the current user has for the current festival that have been voided.
    declare writtenReview int; -- holds if the user has already written a review ( = 1)
    
    set success = 0; -- default, some error

    start transaction; -- transaction only to be atomic
        drop table if exists userFestivalTicket; -- workaround because of lacking permissions to create temporary tables
        
        select `startDate`, now() into s, n from `festivals` where `id` = fID;
        
        -- associates festivalID and userID with ticket status of this user for this festival
        create table `userFestivalTicket`
        select `tt`.`festivalID` as `fID`, `t`.`userID` as `uID`, `t`.`status`
        from `tickets` as `t`
        inner join `ticket_templates` as `tt`
        on `t`.`templateID` = `tt`.`id`
        where
        (`tt`.`festivalID` = fID
        and `t`.`userID` = uID);
        
        -- counts this users total number of tickets for this festival
        select count(*) into hasTickets
        from `userFestivalTicket`;
        
        -- counts this users voided number of tickets for this festival
        select count(*) into hasVoidedTickets
        from `userFestivalTicket`
        where `status` = "voided";
        
        -- checks if the user already posted a review for this festival (returning writtenReview = 1)
        select count(distinct userID) into writtenReview
        from `reviews` as `r`
        inner join `userFestivalTicket` as `uft`
        on (
            `uft`.`uID` = `r`.`userID`
        and `uft`.`fID` = `r`.`festivalID`
        );
        
        drop table if exists userFestivalTicket; -- workaround because of lacking permissions to create temporary tables
        
        if(not (n > s)) then -- festival has not started yet, this can cause all other ticket related errors => first check
            set success = -1;
        elseif(hasTickets < 1) then -- user has no tickets
            set success = -2; 
        elseif(hasVoidedTickets < 1) then -- even if user has tickets, they cant publish a review if none of the tickets are voided
            set success = -3;
        elseif(writtenReview > 0) then -- even if there are voided tickets for this festival in the users account, they cant publish a review if they already have written a review.
            set success = -4;
        else
            set success = 1;
        end if;
        
        if(success = 1) then
            insert ignore into reviews(`festivalID`, `userID`, `festivalRating`, `locationRating`, `text`) values (fID, uID, fR, lR, t);
            select row_count() into success; -- success is number of rows changed
        end if;
        
        if(success > 0) then
            commit;
        else
            rollback;
        end if;
    rollback;
end//

delimiter ;