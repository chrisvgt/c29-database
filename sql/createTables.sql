### FESTIVAL TABLE CREATION ###
use proj_festival;

SET foreign_key_checks = 0; # get rid of the FOREIGN KEY checks to drop existing tables
DROP TABLE IF EXISTS countries, locations, festivals, genres, genresAndFestivals, users, ticket_templates, tickets, reviews;
SET foreign_key_checks = 1;

CREATE TABLE countries(
	`countryCode` VARCHAR(2) NOT NULL,
    `country` VARCHAR(60) NULL,
    PRIMARY KEY (`countryCode`)
);

CREATE TABLE locations(
	`id` INT AUTO_INCREMENT,
    `countryCode` VARCHAR(2) NOT NULL,

    `name` VARCHAR(100) NULL,
    `city` VARCHAR(50) NULL,
    `zipcode` VARCHAR(20) NULL,
    `street` VARCHAR(50) NULL,
    `streetNumber` VARCHAR(11), #bis zu Format: 9997a-9999b
    `longitude` DECIMAL(11,8) NULL,
    `latitude` DECIMAL(11,8) NULL,

    PRIMARY KEY (`id`),
    FOREIGN KEY (`countryCode`)
		REFERENCES countries(`countryCode`)
        ON UPDATE cascade
        ON DELETE CASCADE
);

CREATE TABLE festivals(
	`id` INT AUTO_INCREMENT,
    `locationID` INT NULL,
    
    `name` VARCHAR(50) NULL,
    `startDate` DATETIME NULL,
    `endDate` DATETIME NULL,
    `type` enum('indoor','outdoor') NULL,
    `withCamping` TINYINT(1) NULL,
    
    PRIMARY KEY (`id`),
    FOREIGN KEY (`locationID`)
		REFERENCES locations(`id`)
        ON UPDATE cascade
        on delete set NULL
);

CREATE TABLE genres(
	`name` VARCHAR(50) NOT NULL,
    PRIMARY KEY(`name`)
);

CREATE TABLE genresAndFestivals(
	`festivalID` INT NOT NULL,
    `genre` VARCHAR(50) NOT NULL,
    
    PRIMARY KEY(`festivalID`, `genre`),
    FOREIGN KEY (`festivalID`)
		REFERENCES festivals(`id`)
        ON UPDATE cascade
        ON DELETE CASCADE,
	FOREIGN KEY (`genre`)
		REFERENCES genres(`name`)
        ON UPDATE cascade
        ON DELETE CASCADE
);

CREATE TABLE users(
	`id` INT AUTO_INCREMENT,
    `firstname` VARCHAR(50) NULL,
    `lastname` VARCHAR(50) NULL,
    `email` VARCHAR(50) NULL,
    `password` VARCHAR(255) NULL,
    `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
    `updated_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,

    `countryCode` VARCHAR(2) NOT NULL,
    `dob` date NULL,
    `city` VARCHAR(50) NULL,
    `zipcode` VARCHAR(50) NULL,
    `street` VARCHAR(50) NULL,
    `streetNumber` VARCHAR(11) NULL, #bis zu Format: 9997a-9999b
    
    PRIMARY KEY (`id`),
    FOREIGN KEY (`countryCode`)
		REFERENCES countries(`countryCode`)
        ON UPDATE cascade
        ON DELETE CASCADE
);

CREATE TABLE ticket_templates(
	`id` INT AUTO_INCREMENT,
    `festivalID` INT NOT NULL,
    
    `option` VARCHAR(50),
    `price` DECIMAL(10,2) NULL,
    `validFromDate` DATETIME NULL,
    `expirationDate` DATETIME NULL,
    `totalTickets` INT unsigned,
    `soldTickets` INT unsigned NULL,
    
    PRIMARY KEY (`id`),
    FOREIGN KEY (`festivalID`)
		REFERENCES festivals(`id`)
        ON UPDATE cascade
        ON DELETE CASCADE
);

CREATE TABLE tickets(
	`id` INT AUTO_INCREMENT,
    `userID` INT NOT NULL,
    `templateID` INT NOT NULL,
    
    `purchased_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
    `status` enum('valid', 'voided') NULL,
    
    PRIMARY KEY (`id`),
	FOREIGN KEY (`userID`) REFERENCES users(`id`)
		ON UPDATE cascade
        ON DELETE CASCADE,
    FOREIGN KEY (`templateID`) REFERENCES ticket_templates(`id`)
		ON UPDATE cascade
        ON DELETE CASCADE
);

CREATE TABLE reviews(
	`userID` INT NOT NULL,
    `festivalID` INT NOT NULL,
    
    `festivalRating` TINYINT unsigned NULL,
    `locationRating` TINYINT unsigned NULL,
    `text` VARCHAR(1000) NULL,
    `created_at` DATETIME NULL DEFAULT CURRENT_TIMESTAMP,
    
    PRIMARY KEY (`userID`, `festivalID`),
    FOREIGN KEY (`userID`)
		REFERENCES users(`id`)
		ON UPDATE cascade
        ON DELETE CASCADE,
    FOREIGN KEY (`festivalID`)
		REFERENCES festivals(`id`)
        ON UPDATE cascade
        ON DELETE CASCADE
);